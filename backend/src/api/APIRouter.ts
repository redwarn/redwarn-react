import * as Sentry from "@sentry/node";
import { Router } from "express";
import WMFOAuthDriver from "./WMFOAuthDriver";
import TeyoraDB from "../db/TeyoraDB";
import { TY_BACKEND_IDENTIFIER } from "../../../common/src/Constants";
import RouteDefs from "./routes/RouteDefs";

// This is the main API router, it handles all API points and what they do.
export default class APIRouter {
    private readonly router: Router;
    static wmfOAuth: WMFOAuthDriver;
    private db: TeyoraDB;
    
    constructor(db: TeyoraDB) {
        this.router = Router();
        APIRouter.wmfOAuth = new WMFOAuthDriver();
        this.db = db;
        this.routes();
    }
    
    public getRouter(): Router {
        return this.router;
    }
    
    private routes() {
        // Basic unstructured API endpoints
        this.router.get("/api", (req, res) => {
            res.json({
                message: "Welcome to the Teyora API!",
                version: TY_BACKEND_IDENTIFIER
            });
        });

        // Logout API
        this.router.get("/api/logout", (req, res) => {
            res.clearCookie("DONOTSHARE_teyoraJWT");
            res.redirect("/"); // Redirect to the homepage
        });

        // Everything else...
        Object.keys(RouteDefs).forEach((baseKey) => {
            Object.keys(RouteDefs[baseKey]).forEach((route) => {
                const routeDef = RouteDefs[baseKey][route];
                const routeSplit = route.split(" ");
                const method = routeSplit[0];
                const routeName = routeSplit[1];

                switch (method) {
                    case "GET":
                        this.router.get(`/api/${baseKey}/${routeName}`, async (req, res)=>{
                            (await routeDef).default({
                                db: this.db,
                                req: req,
                                res: res,
                            });
                        });
                        break;
                    case "PUT":
                        this.router.put(`/api/${baseKey}/${routeName}`, async (req, res)=>{
                            (await routeDef).default({
                                db: this.db,
                                req: req,
                                res: res,
                            });
                        });
                        break;
                    case "POST":
                        this.router.post(`/api/${baseKey}/${routeName}`, async (req, res)=>{
                            (await routeDef).default({
                                db: this.db,
                                req: req,
                                res: res,
                            });
                        });
                        break;
                    case "DELETE":
                        this.router.delete(`/api/${baseKey}/${routeName}`, async (req, res)=>{
                            (await routeDef).default({
                                db: this.db,
                                req: req,
                                res: res,
                            });
                        });
                        break;
                    default:
                        Sentry.captureMessage(`Unknown method ${method} for route ${route}`);
                        break;
                }
            });
        });
    }
}