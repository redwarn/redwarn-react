import WorkspaceHelper, { WorkspaceMeta } from "../../../helpers/WorkspaceHelper";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

// Find what workspaces the current user is a member of

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;

            // Get all workspaces the user is a member of
            const WorkspaceMetas: WorkspaceMeta[] = await WorkspaceHelper.getWorkspaces(db, CRI.user.userID);

            // Filter workspaces that are deleted and aren't owned by the user
            const Workspaces: WorkspaceMeta[] = WorkspaceMetas.filter((meta) => {
                return !meta.workspaceIsDeleted && meta.workspaceOwner === CRI.user.userID;
            });

            r.res.status(200).json(Workspaces);
        },
    });
    route.run(props.req, props.res);
};