/*
Get NS Info - this grabs all the details needed for each Wiki in the project like namespaces etc.
*/

import axios from "axios";
import { WorkspaceEntry } from "../../../../db/dbcollections/workspace";
import WorkspaceHelper from "../../../../helpers/WorkspaceHelper";
import { WikiSupport } from "../../../../info/wikis/WikiSupport";
import AuthedRoute, { AuthedRouteExit } from "../../../AuthedRoute";
import { RouteParams } from "../../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;
            const { workspaceID } = r.req.query;
            if (!workspaceID) {
                r.res.status(400).json({
                    error: "No workspace ID provided",
                });
                return;
            }

            // Limit the workspace ID length to prevent DoS
            if (workspaceID.length > 24) {
                r.res.status(403).json({
                    error: "Operation not permitted",
                });
                return;
            }

            try {

                // Let's grab the workspace
                const workspace: WorkspaceEntry = await WorkspaceHelper.getWorkspace(
                    db,
                    workspaceID as string,
                    CRI.user.userID
                );

                if (workspace.workspaceIsLocked) {
                    r.res.status(403).json({
                        error: "Workspace is locked",
                    });
                    return;
                }

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                const wikiIDs: string[] = workspace.workspaceConfig.wikis;

                // If wikiIDs is empty, we can't do anything
                if (!wikiIDs || wikiIDs.length === 0) {
                    r.res.status(200).json({});
                    return;
                }

                // Enforce the limit of max 15 wikis to each workspace
                if (wikiIDs.length > 15) {
                    r.res.status(400).json({
                        error: "Too many wikis in workspace",
                    });
                    return;
                }
                // wikiID: { nsID: { title, prefix } }
                const namespaceResults: Record<string, Record<string, {
                    title: string,
                    prefix: string,
                }>> = {};

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                const queue: Promise<void>[] = wikiIDs.map(wikiID => {
                    return (async () => {
                        // Get the WikiInfo for the wiki
                        const wikiInfo = WikiSupport[wikiID];
                        if (!wikiInfo) {
                            // We obviously can't run on a wiki we have no record of
                            return;
                        }

                        // Make a request to get the namespace info
                        const wikiResultsRaw = (await axios.get(`${wikiInfo.url}/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json`)).data;
                        const wikiResults = wikiResultsRaw.query.namespaces;

                        for (const namespaceID in wikiResults) {
                            if (namespaceID === "0") {
                                // Skip the main namespace, this is handled by the frontend
                                continue;
                            }

                            const wikiNamespace = wikiResults[namespaceID];
                            if (!namespaceResults[wikiID]) {
                                namespaceResults[wikiID] = {};
                            }

                            namespaceResults[wikiID][namespaceID] = {
                                title: wikiNamespace["*"],
                                prefix: wikiNamespace.canonical,
                            };

                        }
                    })();
                });

                // Wait for all the promises to resolve
                await Promise.all(queue);



                r.res.status(200).json({
                    namespaceResults,
                });
            } catch (error) {
                r.res.status(400).json({
                    error: "Workspace not found, operation not permitted or other error",
                });
            }
        },
    });
    route.run(props.req, props.res);
};