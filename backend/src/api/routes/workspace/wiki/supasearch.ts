import { WorkspaceEntry } from "../../../../db/dbcollections/workspace";
import WorkspaceHelper from "../../../../helpers/WorkspaceHelper";
import { WikiSupport } from "../../../../info/wikis/WikiSupport";
import APIRouter from "../../../APIRouter";
import AuthedRoute, { AuthedRouteExit } from "../../../AuthedRoute";
import { RouteParams } from "../../RouteDefs";

/**
 * Supasearch
 * Searches all the Wikis in the configuration for the correct workspace to find pages quickly
 * Requires: query, workspaceID
 */

export interface SupaSearchResult {
    page: string;
    pID: number; // Page ID
    snippet: string | null;
    timestamp: string;
}

export type SupasearchResults = { [wikiID: string] : {
    [namespace: string] : SupaSearchResult[]; // Array of results in the namespace
}};

export interface MWAPIResponseContent {
    ns: number;
    title: string;
    snippet: string | null;
    pageid: number;
    size: number;
    timestamp: string;
    wordcount: number;
}

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;
            const { workspaceID, searchQuery } = r.req.body;
            if (!workspaceID || !searchQuery) {
                r.res.status(400).json({
                    error: "No workspace ID or search query provided",
                });
                return;
            }

            // Limit the workspace ID length to prevent DoS
            if (workspaceID.length > 24) {
                r.res.status(403).json({
                    error: "Operation not permitted",
                });
                return;
            }

            // Limit query to 100 characters
            if (searchQuery.length > 100) {
                r.res.status(403).json({
                    error: "Operation not permitted",
                });
                return;
            }

            try {

                // Let's grab the workspace
                const workspace: WorkspaceEntry = await WorkspaceHelper.getWorkspace(
                    db,
                    workspaceID as string,
                    CRI.user.userID
                );

                if (workspace.workspaceIsLocked) {
                    r.res.status(403).json({
                        error: "Workspace is locked",
                    });
                    return;
                }

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                const wikiIDs: string[] = workspace.workspaceConfig.wikis;

                // If wikiIDs is empty, we can't do anything
                if (!wikiIDs || wikiIDs.length === 0) {
                    r.res.status(400).json({
                        error: "Add at least one wiki to the workspace silly!",
                    });
                    return;
                }

                // Enforce the limit of max 15 wikis to each workspace
                if (wikiIDs.length > 15) {
                    r.res.status(400).json({
                        error: "Too many wikis in workspace",
                    });
                    return;
                }

                // Record for search result storage
                const wikiSearchResults:SupasearchResults = {};

                // We need an authed Axios as some search results may require authentication
                const authxios = APIRouter.wmfOAuth.getAuthedAxios(CRI.WMFOAuthToken);


                // Let's build the queue of promisses
                const queue: Promise<void>[] = wikiIDs.map(wikiID => {
                    return (async () => {
                        // Get the WikiInfo for the wiki
                        const wikiInfo = WikiSupport[wikiID];
                        if (!wikiInfo) {
                            // We obviously can't run on a wiki we have no record of
                            return;
                        }

                        // Make a request to search the wiki
                        const wikiResultsRaw = (await authxios.get(`${wikiInfo.url}/w/api.php?action=query&list=search&srsearch=${
                            encodeURIComponent(searchQuery)}&srnamespace=*&format=json`)).data;
                        
                        // Parse the results
                        const wikiResults = wikiResultsRaw.query.search;

                        // If we don't have a result for the wiki, create an empty array
                        if (!wikiSearchResults[wikiID]) {
                            wikiSearchResults[wikiID] = {};
                        }

                        // For each result
                        wikiResults.forEach((result : MWAPIResponseContent) => {
                            // If we don't have a result for the namespace, create an empty array
                            if (!wikiSearchResults[wikiID][result.ns]) {
                                wikiSearchResults[wikiID][result.ns] = [];
                            }
                            // Add the result to the array
                            wikiSearchResults[wikiID][result.ns].push({
                                page: result.title,
                                pID: result.pageid,
                                snippet: result.snippet,
                                timestamp: result.timestamp,
                            });
                        });
                    })();
                });

                await Promise.all(queue);

                r.res.status(200).json({
                    wikiSearchResults,
                });
            } catch (error) {
                r.res.status(400).json({
                    error: "Workspace not found, operation not permitted or other error",
                });
            }
        },
    });
    route.run(props.req, props.res);
};