import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: true,
            LockedUsersCanRun: true,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            r.res.status(200).json(r.CRI.user);
        },
    });
    route.run(props.req, props.res);
};