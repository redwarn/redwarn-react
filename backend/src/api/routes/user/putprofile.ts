import { ProfileEntry, UserProfile } from "../../../db/dbcollections/profile";
import UserAuth from "../../../helpers/UserAuth";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false, // Users must be unlocked before they can move forward
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { req, res, db, CRI } = r;
            // Get new profile
            const newProfile:UserProfile = req.body;

            // Verify the new profile
            // Bio checks: Length must be less than 241 characters, user must be approved
            newProfile.bio = (newProfile.bio && (newProfile.bio.length < 241 && CRI.user.isApproved) ? newProfile.bio : "").trim();

            // Nick name checks: Length must be less than 41 characters, user must be approved, else
            // nickname will be set to the username (this is done in ui)
            newProfile.nickname = (newProfile.nickname && (newProfile.nickname.length < 41 && CRI.user.isApproved)
                ? newProfile.nickname : "").trim();

            // Avatar checks: Length must be less than 35 characters, else empty
            newProfile.profilePicture = (newProfile.profilePicture && newProfile.profilePicture.length < 35 ? newProfile.profilePicture : "").trim();

            // Update the user
            
            const profile = await UserAuth.setUserProfileNPV(db, CRI.user, {
                username: CRI.user.username, // We need to generate a profile entry
                userID: CRI.user.userID,
                profile: newProfile
            } as ProfileEntry);

            res.status(200).json(profile);
        },
    });
    route.run(props.req, props.res);
};