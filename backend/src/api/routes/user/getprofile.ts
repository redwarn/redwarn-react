import UserAuth from "../../../helpers/UserAuth";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false, // Users must be unlocked before they can move forward
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const profile = await UserAuth.getUserProfileNPV(r.db, r.CRI.user);
            r.res.status(200).json(profile);
        },
    });
    route.run(props.req, props.res);
};