// Contains all routes for the api
// /api/{key}/{value}
// Put your protocol at the start of the url, for example GET wmf/redirect
import { Response, Request } from "express";
import TeyoraDB from "../../db/TeyoraDB";

// Routes

export interface RouteParams {
    res: Response,
    req: Request,
    db: TeyoraDB
}

const RouteDefs:Record <string, Record <string, Promise<any>>> = { // todo: fix the "any" here
    "info": {
        // Pulls the definitions of support for all supported wikis
        "GET wikisupport": (import("./info/wikisupport")),
    },

    "oauth" : {
        // OAuth - Redirect to the WMF OAuth page
        "GET wmf/redirect": (import("./oauth/wmfloginredirect")),
        // OAuth - Callback from WMF OAuth page
        "GET wmf/callback": (import("./oauth/wmflogincallback")),
    },

    "user": {
        // Get the user's preferences
        "GET preferences": (import("./user/getpreferences")),
        // Set the user's preferences
        "PUT preferences": (import("./user/putpreferences")),
        // Get the user's profile
        "GET profile": (import("./user/getprofile")),
        // Set the user's profile
        "PUT profile": (import("./user/putprofile")),
        // Get the user's user data
        "GET info": (import("./user/info")),
        // Unlock or activate a user's account
        "GET unlock": (import("./user/unlock")),
    },

    "workspace": {
        // Create a new workspace
        "PUT create": (import("./workspace/create")),
        // Get an array of all workspaces the authed user has access to
        "GET joined": (import("./workspace/joined")),
        // Get the content of a workspace
        "GET get": (import("./workspace/get")),
        // Search Wikis in a workspace
        "PUT wiki/supasearch": (import("./workspace/wiki/supasearch")),
        // Get namespace infos for all wikis in a workspace
        "GET wiki/nsinfo": (import("./workspace/wiki/nsinfo")),
        // Get Wiki page HTML content
        "GET wiki/getpagehtml": (import("./workspace/wiki/getpagehtml")),
        // Get Wiki CSS
        "GET wiki/getcss": (import("./workspace/wiki/getcss")),
    },
};

export default RouteDefs;