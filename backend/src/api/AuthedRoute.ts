// Provides an authenticated route for requests

import TeyoraDB from "../db/TeyoraDB";
import UserAuth, { UserSecurityRequirements } from "../helpers/UserAuth";
import { Request, Response } from "express";
import * as Sentry from "@sentry/node";
import { CurrentRequestInfo } from "../helpers/JWTVerify";

// Used to create a route that requires authentication
export interface AuthedRouteEntry {
    db: TeyoraDB,
    security: UserSecurityRequirements,
    // eslint-disable-next-line no-unused-vars
    okayRoute: (reqInfo: AuthedRouteExit) => Promise<void>,
}

// Used on exit
export interface AuthedRouteExit {
    db: TeyoraDB,
    res: Response,
    req: Request,
    CRI: CurrentRequestInfo,
}

export default class AuthedRoute {
    private route: AuthedRouteEntry;
    constructor(route: AuthedRouteEntry) {
        this.route = route;
    }

    public async run(req: Request, res: Response) {
        const { db, security, okayRoute } = this.route;
        try {
            const canRun = await UserAuth.getAndVerifyRequestInfo(db, req, res, security);
            if (typeof canRun == "boolean") throw new Error("unauth (typeof canRun is bool)"); // Unauthorized

            try {
                // Send the request through
                await okayRoute({
                    db,
                    res,
                    req,
                    CRI: canRun,
                });
            } catch (error) {
                // An issue occured
                Sentry.captureException(error);
                res.status(500).json({
                    message: "An internal error occured and logged",
                    error: error.message
                });
            }
            
        } catch (error) {
            if (res.destroyed) return; // Response already sent
            res.status(403).json({
                error: "Forbidden",
                message: "You don't have sufficent permissions for this endpoint. You may need to log in again.",
                details: error.message,
            });
        }
    }
}