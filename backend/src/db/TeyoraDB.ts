// Connects and interfaces with CouchDB
// This is very much raw and shouldn't handle any logic, security, etc.

import Nano from "nano";
import { TY_BACKEND_COUCHDB_PASSWORD } from "../../../common/src/backend/BackendSecretConstants";

// Import all dbcollections here
import { AdminLogEntry } from "./dbcollections/adminlog";
import { ConfigEntry } from "./dbcollections/config";
import { ProfileEntry } from "./dbcollections/profile";
import { RevisionEntry } from "./dbcollections/revision";
import { SharedWatchlistEntry } from "./dbcollections/sharedwatchlist";
import { UserEntry } from "./dbcollections/user";
import { WarningEntry } from "./dbcollections/warninglog";
import { WorkspaceEntry } from "./dbcollections/workspace";

export default class TeyoraDB {
    public AdminLog: Nano.DocumentScope<AdminLogEntry>;
    public Config: Nano.DocumentScope<ConfigEntry>;
    public Profile: Nano.DocumentScope<ProfileEntry>;
    public Revision: Nano.DocumentScope<RevisionEntry>;
    public SharedWatchlist: Nano.DocumentScope<SharedWatchlistEntry>;
    public User: Nano.DocumentScope<UserEntry>;
    public WarningLog: Nano.DocumentScope<WarningEntry>;
    public Workspace: Nano.DocumentScope<WorkspaceEntry>;
    
    async setup(CouchDB: Nano.ServerScope): Promise<void> {
        // Create the databases
        await Promise.all([
            CouchDB.db.create("adminlog"),
            CouchDB.db.create("config"),
            CouchDB.db.create("profile"),
            CouchDB.db.create("revision"),
            CouchDB.db.create("sharedwatchlist"),
            CouchDB.db.create("warninglog"),
            CouchDB.db.create("workspace"),
            CouchDB.db.create("user")
        ]);
    }

    // Connection function
    public async connect(): Promise<void> {
        
        // Connect to the database and establish schemas etc.
        const CouchDB = Nano(`http://teyorabackend:${TY_BACKEND_COUCHDB_PASSWORD}@127.0.0.1:6767`);
        try {

            this.AdminLog = await CouchDB.db.use("adminlog");
            this.Config = await CouchDB.db.use("config");
            this.Profile = await CouchDB.db.use("profile");
            this.Revision = await CouchDB.db.use("revision");
            this.SharedWatchlist = await CouchDB.db.use("sharedwatchlist");
            this.WarningLog = await CouchDB.db.use("warninglog");
            this.Workspace = await CouchDB.db.use("workspace");
            this.User = await CouchDB.db.use("user");

            // Generate an action ID which is a random string for this start
            const actionID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            await this.AdminLog.insert(new AdminLogEntry(
                actionID,
                "TeyoraDB",
                "SuperOverlord",
                "Server started - TeyoraDB connected successfully",
                "TeyoraDB",
                new Date(),
                "",
                {},
                false
            ));

        } catch (error) {
            if (error.reason === "Database does not exist.") {
                // We can setup the database here
                console.log("Databases do not exist, creating...");
                await this.setup(CouchDB);
                throw new Error("Databases have been created, please restart the server :)");
            } else {
                throw error;
            }
        }
    }
}