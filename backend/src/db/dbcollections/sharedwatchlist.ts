import Nano from "nano";
export interface iSharedWatchlistEntry extends Nano.MaybeDocument {
    teyoraSharedWatchlistID: string, // The shared watchlist ID, used to identify the shared watchlist on Teyora
    sharedWatchlistOwner: string, // The userID of the user who owns the shared watchlist
    sharedWatchlistName: string, // The name of the shared watchlist
    sharedWatchlistDescription: string, // The description of the shared watchlist
    sharedWatchlistParticipants: Array<any>, // The user ID participants of the shared watchlist
    sharedWatchlistIcon: string, // The icon of the shared watchlist
    sharedWatchlistConfig: any, // The config of the shared watchlist
    sharedWatchlistCreationDate: Date, // The date the shared watchlist was created
    sharedWatchlistLastModified: Date, // The date the shared watchlist was last modified
    sharedWatchlistLastActive: Date, // The date the shared watchlist was last active
    sharedWatchlistIsLocked: boolean, // Whether the shared watchlist is locked or not
    sharedWatchlistIsPublic: boolean, // Whether the shared watchlist is public or not
    sharedWatchlistIsDeleted: boolean, // Whether the shared watchlist is deleted or not
    sharedWatchlistIsJoinable: boolean, // Whether the shared watchlist is joinable or invite only
}


export class SharedWatchlistEntry implements iSharedWatchlistEntry {
    _id?: string;
    _rev?: string;
    teyoraSharedWatchlistID: string;
    sharedWatchlistOwner: string;
    sharedWatchlistName: string;
    sharedWatchlistDescription: string;
    sharedWatchlistParticipants: Array<any>;
    sharedWatchlistIcon: string;
    sharedWatchlistConfig: any;
    sharedWatchlistCreationDate: Date;
    sharedWatchlistLastModified: Date;
    sharedWatchlistLastActive: Date;
    sharedWatchlistIsLocked: boolean;
    sharedWatchlistIsPublic: boolean;
    sharedWatchlistIsDeleted: boolean;
    sharedWatchlistIsJoinable: boolean;
    
    constructor(
        teyoraSharedWatchlistID: string,
        sharedWatchlistOwner: string,
        sharedWatchlistName: string,
        sharedWatchlistDescription: string,
        sharedWatchlistParticipants: Array<any>,
        sharedWatchlistIcon: string,
        sharedWatchlistConfig: any,
        sharedWatchlistCreationDate: Date,
        sharedWatchlistLastModified: Date,
        sharedWatchlistLastActive: Date,
        sharedWatchlistIsLocked: boolean,
        sharedWatchlistIsPublic: boolean,
        sharedWatchlistIsDeleted: boolean,
        sharedWatchlistIsJoinable: boolean
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.teyoraSharedWatchlistID = teyoraSharedWatchlistID;
        this.sharedWatchlistOwner = sharedWatchlistOwner;
        this.sharedWatchlistName = sharedWatchlistName;
        this.sharedWatchlistDescription = sharedWatchlistDescription;
        this.sharedWatchlistParticipants = sharedWatchlistParticipants;
        this.sharedWatchlistIcon = sharedWatchlistIcon;
        this.sharedWatchlistConfig = sharedWatchlistConfig;
        this.sharedWatchlistCreationDate = sharedWatchlistCreationDate;
        this.sharedWatchlistLastModified = sharedWatchlistLastModified;
        this.sharedWatchlistLastActive = sharedWatchlistLastActive;
        this.sharedWatchlistIsLocked = sharedWatchlistIsLocked;
        this.sharedWatchlistIsPublic = sharedWatchlistIsPublic;
        this.sharedWatchlistIsDeleted = sharedWatchlistIsDeleted;
        this.sharedWatchlistIsJoinable = sharedWatchlistIsJoinable;
    }

    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}