import Nano from "nano";
export interface iWarningEntry extends Nano.MaybeDocument {
    teyoraWarningID: string, // The warning ID, used to identify the warning on Teyora
    warningType: string, // The type of warning, e.g. "Spam"
    warningActor: string, // The userID of the user who submitted the warning
    associatedRevisions: Array<any>, // The teyoraRevisionIDs associated with the warning
    warningInfo: any, // The information about the warning, e.g. the user who submitted the warning
    warningDate: Date, // The date the warning was submitted
    undone: boolean, // Whether the warning has been undone or not
    undoDate: Date, // The date the warning was undone
    undoInfo: any, // The information about the undo, e.g. the user who undid the warning
}

export class WarningEntry implements iWarningEntry {
    _id?: string;
    _rev?: string;
    teyoraWarningID: string;
    warningType: string;
    warningActor: string;
    associatedRevisions: Array<any>;
    warningInfo: any;
    warningDate: Date;
    undone: boolean;
    undoDate: Date;
    undoInfo: any;
    
    constructor(
        teyoraWarningID: string,
        warningType: string,
        warningActor: string,
        associatedRevisions: Array<any>,
        warningInfo: any,
        warningDate: Date,
        undone: boolean,
        undoDate: Date,
        undoInfo: any
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.teyoraWarningID = teyoraWarningID;
        this.warningType = warningType;
        this.warningActor = warningActor;
        this.associatedRevisions = associatedRevisions;
        this.warningInfo = warningInfo;
        this.warningDate = warningDate;
        this.undone = undone;
        this.undoDate = undoDate;
        this.undoInfo = undoInfo;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}