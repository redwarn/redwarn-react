// Handles logging admin events
import TeyoraDB from "../db/TeyoraDB";
import { iUserEntry } from "../db/dbcollections/user";
import { AdminLogEntry } from "../db/dbcollections/adminlog";

export default class AdminLog {

    // Create a new log event
    // Special perms aren't required for log events, but they shouldn't
    // be called directly to prevent abuse and use generic terms for internationalization
    public static createLogEvent(db: TeyoraDB, actor: iUserEntry, target: iUserEntry, isPublic: boolean, source: string, action: string, reason?: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const newID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            db.AdminLog.insert(new AdminLogEntry(newID, actor.username, actor.userID, action, source, new Date(), target.username, reason, isPublic)).then((response) => {
                resolve(newID);
            }, (error) => {
                reject(error);
            });
        });
    }

}