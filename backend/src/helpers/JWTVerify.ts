// Verifies JWTs
import { TY_BACKEND_JWT_SECRET } from "../../../common/src/backend/BackendSecretConstants";
import { UserEntry } from "../db/dbcollections/user";
import UserAuth from "./UserAuth";
import jwt from "jsonwebtoken";
import { Request, Response } from "express";
import TeyoraDB from "../db/TeyoraDB";
import { TY_CURRENT_BASE_URL } from "../../../common/src/backend/BackendConstants";
import { OAuthTokens } from "../api/WMFOAuthDriver";

export interface CurrentRequestInfo {
    user: UserEntry,
    WMFOAuthToken: OAuthTokens,
}
export default class JWTVerify {

    public static pullJWT(req: Request, res: Response): string {
        const jwtToken = req.cookies.DONOTSHARE_teyoraJWT;
        if (!jwtToken) {
            res.status(401).json({
                message: "Your request couldn't be authenticated, please log in again.",
            });
            return "ERROR";
        }
        return jwtToken;
    }

    public static newJWT(db: TeyoraDB, user: UserEntry, WMFOAuthToken: OAuthTokens): Promise<string> {
        return new Promise((resolve) => {
            const jwtToken = jwt.sign({
                tokenID: Math.random().toString(36).substring(2, 12), // for token blacklisting (not used yet)
                userID: user.userID,
                username: user.username,
                WMFOAuthToken: WMFOAuthToken,
                dateIssued: new Date().toISOString(),
            }, TY_BACKEND_JWT_SECRET, {
                expiresIn: "12h",
            });
            resolve(jwtToken);
        });
    }

    public static verify(db: TeyoraDB, req: Request): Promise<CurrentRequestInfo> {
        return new Promise((resolve, reject) => {
            const jwtToken = req.cookies.DONOTSHARE_teyoraJWT;

            if (typeof jwtToken !== "string") {
                // No JWT, no user
                reject(new Error("You're not logged in, please go back to the homepage and log in."));
                return;
            }

            // not sure on the types here, but we shouldn't have issues
            jwt.verify(jwtToken, TY_BACKEND_JWT_SECRET, async (err: any, decoded: any) => {
                if (err) {
                    // JWT is invalid, no user
                    reject(err);
                    return;
                }

                // JWT is valid, get the user info (use NPV as this is only for this users eyes)
                const user = await UserAuth.getUserNPV(db, decoded.userID);

                if (user == null) {
                    // User doesn't exist, no user, which is weird, so handle it
                    reject(new Error(`Hmmm... something weird has happened. The logged in user doesn't exist, please log out at ${TY_CURRENT_BASE_URL}/api/logout and log in again.`));
                    return;
                }

                resolve({
                    user: user,
                    WMFOAuthToken: decoded.WMFOAuthToken,
                });
            });
        });
    }
}

