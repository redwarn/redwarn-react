# Teyora
Teyora's core app - made with Typescript and React.


This repository contains both frontend and backend. You can find them in their respective folders (`frontend`, and `backend`). Both ends rely on the `common` folder for common definitions.

## Contributors
Teyora is primarily maintained and developed by [@ed_E](https://gitlab.com/ed_e)** ([\[\[User:Ed6767\]\]](https://en.wikipedia.org/wiki/User:Ed6767))

However, it wouldn't have been possible without the ground work laid out for RedWarn React by the ([original RedWarn team](https://en.wikipedia.org/wiki/Wikipedia:RedWarn)).

The dark mode when viewing a page is thanks to contributors to the Wikimedia Design teams' [Dark Mode](https://en.wikipedia.org/wiki/User:Volker_E._(WMF)/dark-mode) gadget. All other styles loaded in the page are pulled from the active Wiki - see there for attribution.

Development has been assisted by GitHub Copilot.

## Development
External contributions aren't being taken at this time as Teyora is still being planned. Check back soon.