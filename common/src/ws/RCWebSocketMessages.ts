import { TeyoraEditInfo } from "../objects/TeyoraEditInfo";
import { ServerToClientMessage } from "./WebSocketMessage";

export type RCWebSocketMessages =
    /* Server to Client messages */
    ServerRecentChange

    /* Client to Server messages */
    // n/a atm - todo: maybe subscribe/remove wiki? or just disconnect and reconnect


export interface ServerRecentChange extends ServerToClientMessage {

    type: "liveEdit";
    change: TeyoraEditInfo;

}