import {RCWebSocketMessages} from "./RCWebSocketMessages";
import {TechnicalWebSocketMessages} from "./TechnicalWebSocketMessages";

type Message =
    RCWebSocketMessages
    | TechnicalWebSocketMessages;

// Type checking and management.
// Courtesy of Leijurv - who wrote this moments after learning TypeScript
// What a madlad...

type Filter<T, U> = T extends U ? T : never;
export type DirectedMessage<Dir> = Filter<Message, Dir>;
type MessageType<Dir> = DirectedMessage<Dir>["type"];
type MessageMap<Dir, V> = { [K in MessageType<Dir>]: V extends { type: K } ? V : never};
type MessageTypeMap<Dir> = MessageMap<Dir, DirectedMessage<Dir>>;
type ServerClientPattern<Output> = { [K in keyof MessageTypeMap<ServerToClientMessage>]: (msg: MessageTypeMap<ServerToClientMessage>[K]) => Output};
type ClientServerPattern<Output> = { [K in keyof MessageTypeMap<ClientToServerMessage>]: (msg: MessageTypeMap<ClientToServerMessage>[K]) => Output};

export function messageMatcherServerToClient<Output>(pattern: ServerClientPattern<Output>): (msg: DirectedMessage<ServerToClientMessage>) => Output {
    return msg => pattern[msg.type](msg as any);
}
export function messageMatcherClientToServer<Output>(pattern: ClientServerPattern<Output>): (msg: DirectedMessage<ClientToServerMessage>) => Output {
    return msg => pattern[msg.type](msg as any);
}

interface TypedMessage {
    type: string;
}

export enum Direction {
    ClientToServer,
    ServerToClient
}

export interface ClientToServerMessage extends TypedMessage {
    direction: Direction.ClientToServer;
}
export interface ServerToClientMessage extends TypedMessage {
    direction: Direction.ServerToClient;
}