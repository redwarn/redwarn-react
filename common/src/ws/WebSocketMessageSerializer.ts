import { ServerToClientMessage, ClientToServerMessage, DirectedMessage } from "./WebSocketMessage";

export default class WebSocketMessageSerializer {

    static serializeServer(message : DirectedMessage<ServerToClientMessage>) : string{
        return JSON.stringify(message);
    }

    static deserializeServer(message : string) : DirectedMessage<ClientToServerMessage> {
        return JSON.parse(message);
    }

    static serializeClient(message : DirectedMessage<ClientToServerMessage>) : string{
        return JSON.stringify(message);
    }

    static deserializeClient(message : string) : DirectedMessage<ServerToClientMessage> {
        return JSON.parse(message);
    }

}