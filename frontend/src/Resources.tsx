/** The Teyora wordmark. **/
export const TeyoraWordmark = "images/TY_Wordmark_Colour.svg";
/** The Teyora wordmark, but for dark backgrounds. **/
export const TeyoraWordmarkDark = "images/TY_Wordmark.svg";
/** The Teyora wordmark, but completely white. **/
export const TeyoraWordmarkWhite = "images/TY_Wordmark_White.svg";