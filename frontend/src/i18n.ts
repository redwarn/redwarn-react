import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import HttpBackend from "i18next-http-backend";
import { TY_DEBUG } from "../../common/src/Constants";

// Put all namespaces that need to be loaded here.
const namespaces = ["ui", "login", "errors", "patrol"];

interface Language {
    code: string;
    name: string;
}

// List of locales/languages available, in order.
// Don't forget to make sure this matches with translation.json
export const languageList: Language[] = [
    {
        code: "en-GB",
        name: "English (United Kingdom)",
    },
    {
        code: "en-US",
        name: "English (United States)",
    },
    {
        code: "es-ES",
        name: "Español (España)",
    },
];

export async function initLangs() {
    // if for whatever reason languageList has qqq in it, remove it
    if (languageList.find((l) => l.code === "qqq")) {
        languageList.splice(
            languageList.findIndex((l) => l.code === "qqq"),
            1
        );
    }
    await i18n
        .use(LanguageDetector)
        .use(HttpBackend)
        .use(initReactI18next)
        .init({
            backend: {
                loadPath: "/lang/{{lng}}/{{ns}}.json",
            },
            debug: TY_DEBUG === "development",
            fallbackLng: "en-GB",
            interpolation: {
                escapeValue: false, // not needed for react as it escapes by default
            },
        });

    // Load all the namespaces.
    await i18n.loadNamespaces(namespaces);
}

export default i18n;
