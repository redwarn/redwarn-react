import Teyora from "../App";
import i18n from "../i18n";
import { LocalisedButtons } from "../ui/LocalisedButtons";
import TeyoraUI from "../ui/TeyoraUI";
import { iFeed } from "../ui/workspace/patrol/feeddrawer/Feed";
import FeedExtension from "./tef/FeedExtension";

export interface WorkspaceMeta {
    teyoraWorkspaceID: string;
    workspaceOwner: string;
    workspaceName: string;
    workspaceDescription: string;
    workspaceParticipants: string[];
    workspaceLastActive: Date;
    workspaceIsLocked: boolean;
    workspaceIsPublic: boolean;
    workspaceIsDeleted: boolean;
    lastOpenedByUser?: Date;
}

// This is the workspace object
export interface Workspace {
    meta: WorkspaceMeta;
    workspaceConfig: Record<string, any>;

    feeds: iFeed[],
}

export enum WorkspaceConfigScope {
    Default = "default", // Automatically according to below hierarchy
    UserWorkspace = "workspace", // User specific overrides for this workspace
    UserGlobal = "global", // For every workspace this user opens
    Workspace = "workspace", // Workspace default settings
}

export type NSInfoAPIResponse = Record<string, Record<string, {
    title: string;
    prefix: string;
}>>;

export default class WorkspaceManager {
    // Unlike others, this is only loaded when needed and not on load

    public workspaces: WorkspaceMeta[] = [];
    public hasLoaded = false;
    public currentWorkspace: Workspace | null = null;
    public FeedExtensions: Record<string, FeedExtension> = {};

    // Namespace details
    public namespaceInfo:NSInfoAPIResponse = {};

    // eslint-disable-next-line no-unused-vars
    public onLoadFinishedHandler: (workspace: Workspace) => void;
    // eslint-disable-next-line no-unused-vars
    public onLoadStatusChanged: (newState: string) => void;
    // eslint-disable-next-line no-unused-vars
    public onLoadError: (error: string) => void;


    public async fetchWorkspaceMeta(): Promise<WorkspaceMeta[]> {
        const fR = await fetch("/api/workspace/joined");
        if (fR.status !== 200) {
            throw new Error("Failed to fetch workspace meta");
        }
        
        this.hasLoaded = true;
        // eslint-disable-next-line prefer-const
        let workspacesToSet = await fR.json() as WorkspaceMeta[];

        // For each workspace, add the last accessed date
        for (const workspace of workspacesToSet) {
            const lastOpenedPref = Teyora.TY.PreferencesManager.getPreference(`workspaceAccess/${workspace.teyoraWorkspaceID}`, null);
            if (lastOpenedPref) {
                workspace.lastOpenedByUser = new Date(lastOpenedPref);
            }
            workspace.workspaceLastActive = new Date(workspace.workspaceLastActive);
        }

        // Sort the workspaces by last accessed date
        workspacesToSet.sort((a, b) => {
            if (a.lastOpenedByUser.getTime == null && b.lastOpenedByUser.getTime == null)
                return 0;

            if (a.lastOpenedByUser == null)
                return 1;

            if (b.lastOpenedByUser == null)
                return -1;

            return b.lastOpenedByUser.getTime() - a.lastOpenedByUser.getTime();
        });

        this.workspaces = workspacesToSet;
        return this.workspaces;
    }

    public async newWorkspace(workspaceTitle: string, workspaceDescription: string): Promise<string> {
        if (!Teyora.TY.CurrentUser.isApproved) throw new Error("Approval is required to create a workspace");

        if (!this.hasLoaded) {
            await this.fetchWorkspaceMeta();
            if (!this.hasLoaded) throw new Error("Failed to load workspace meta");
        }

        // Basic validation
        if (workspaceTitle.length > 50 ||
            workspaceTitle.length < 10 ||
            workspaceDescription.length > 1500 ||
            workspaceDescription.length < 10) {
            throw new Error("Invalid workspace title or description");
        }

        // Do not create if we are in more than 100 workspaces as we would go over the limit
        if (this.workspaces.length >= 100) {
            TeyoraUI.showAlertBox(i18n.t("ui:workspaceSelector.create.maxWorkspaces"), i18n.t("ui:workspaceSelector.create.maxWorkspacesTitle"));
            throw new Error("You have reached the workspace limit");
        }

        // Create the workspace
        try {
            const fR = await fetch("/api/workspace/create", {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    title: workspaceTitle,
                    description: workspaceDescription
                })
            });

            if (fR.status !== 200) {
                throw new Error("Failed to create workspace (server error?)");
            }

            // Update the values
            const newID = (await fR.json() as Record<string, string>).id;

            // Add last accessed date to prefs
            Teyora.TY.PreferencesManager.setPreference(`workspaceAccess/${newID}`, new Date());
            await Teyora.TY.PreferencesManager.commitPreferences();

            return newID;

        } catch (error) {
            const { t } = i18n;
            // Inform the user that the preferences couldn't be fetched and ask them to refresh
            // or log out and back in again
            console.error(error);
            await TeyoraUI.showAlertBox(t("ui:workspaceSelector.create.error"), t("ui:savePreferencesError.errorTitle"));
            throw new Error("Failed to create workspace"); // Stop the promise from resolving
        }
    }

    public async leaveOrDeleteWorkspace(workspaceID: string): Promise<void> {
        const { t } = i18n;
        const lB = LocalisedButtons();
        const meta = this.workspaces.find(w => w.teyoraWorkspaceID === workspaceID);
        if (!meta) {
            throw new Error("Failed to find workspace");
        }

        // If owner is not the current user, then we can just leave the workspace
        if (meta.workspaceOwner !== Teyora.TY.CurrentUser.userID) {
            // Confirm the user wants to leave
            const confirmLeave = await TeyoraUI.showAlertBox(t("ui:workspaceSelector.leave.confirm"), t("ui:workspaceSelector.leave.confirmTitle", { workspaceTitle: meta.workspaceName }), [
                lB.CANCEL,
                lB.OK
            ]);
        } else {
            // Confirm the user wants to delete
            const confirmDelete = await TeyoraUI.showAlertBox(t("ui:workspaceSelector.delete.confirm"), t("ui:workspaceSelector.delete.confirmTitle", { workspaceTitle: meta.workspaceName }), [
                lB.CANCEL,
                lB.DELETE
            ]);

            if (confirmDelete == lB.CANCEL) return;

            // Confirm one more time
            const confirmeDelete2 = await TeyoraUI.showAlertBox(t("ui:workspaceSelector.delete.confirm2", { memberCount: meta.workspaceParticipants.length }), t("ui:workspaceSelector.delete.confirmTitle", { workspaceTitle: meta.workspaceName }), [
                lB.DELETE,
                lB.CANCEL
            ]);

            if (confirmeDelete2 == lB.CANCEL) return;

            // Delete the workspace
            //TODO

        }
    }

    public async loadWorkspace(): Promise<void> {
        const { t } = i18n;
        try {
            // Get the workspace ID from the URL
            const workspaceID = Teyora.TY.LinkHandler.params[0];
            
            // Fetch workspace contents
            const fR = await fetch(`/api/workspace/get/?workspaceID=${workspaceID}`);

            if (fR.status == 404)
                throw new Error(t("patrol:load.notFound"));
            if (fR.status == 403)
                throw new Error(t("patrol:load.forbidden"));
            if (fR.status != 200) 
                throw new Error(t("patrol:load.serverError"));
            

            // Update last accessed date since this was successful
            this.onLoadStatusChanged(t("patrol:load.updatingLastAccessed"));
            Teyora.TY.PreferencesManager.setPreference(`workspaceAccess/${workspaceID}`, new Date());
            await Teyora.TY.PreferencesManager.commitPreferences();

            this.onLoadStatusChanged(t("patrol:load.loading"));
            const workspaceResponse = (await fR.json()).workspace;

            // Get the workspace details
            const cWorkspace: Workspace = {
                meta: {
                    teyoraWorkspaceID: workspaceResponse.teyoraWorkspaceID,
                    workspaceName: workspaceResponse.workspaceName,
                    workspaceDescription: workspaceResponse.workspaceDescription,
                    workspaceOwner: workspaceResponse.workspaceOwner,
                    workspaceParticipants: workspaceResponse.workspaceParticipants,
                    lastOpenedByUser: new Date(workspaceResponse.lastOpenedByUser),
                    workspaceLastActive: new Date(workspaceResponse.workspaceLastActive),
                    workspaceIsDeleted: workspaceResponse.workspaceIsDeleted,
                    workspaceIsLocked: workspaceResponse.workspaceIsLocked,
                    workspaceIsPublic: workspaceResponse.workspaceIsPublic,
                },
                workspaceConfig: workspaceResponse.workspaceConfig,

                feeds : [
                    // Todo later
                    // {
                    //     id: "0",
                    //     feedExtensionID: "FeedTest",
                    //     title: "Test Feed",
                    //     launchMessages: [],
                    //     wikis: ["enwiki"],
                    //     filters: [],
                    //     maxLength: 50,
                    //     //soundOnNewEdit: "dong",
                    // }
                ]
            };

            this.currentWorkspace = cWorkspace;

            // Update the UI
            this.onLoadStatusChanged(t("patrol:load.requestingNSInfo"));
            // For each wiki, we need to grab appropriate info about namespaces and pages
            const nsInfoReq = await (fetch(`/api/workspace/wiki/nsinfo?workspaceID=${workspaceID}`) as Promise<Response>);
            const nsInfo: NSInfoAPIResponse = (await nsInfoReq.json()).namespaceResults;
            // Add info for namespace 0 (mainspace). What the "name" value is
            // depends on our locale in Teyora so we do this here instead of on the backend
            for (const wiki in nsInfo) {
                nsInfo[wiki]["0"] = {
                    title: t("patrol:wiki.mainspaceTitle"),
                    prefix: "",
                };
            }

            this.namespaceInfo = nsInfo;

            // So we can display pages correctly, we need to pull the stylesheet URLs for each wiki.
            // These will be loaded into the iframes later
            this.onLoadStatusChanged(t("patrol:load.requestingCSS"));

            // TODO: Load and setup extensions here

            // For feed extensions, we don't init them as this is done by each feed and then subscribed to
            this.FeedExtensions["FeedTest"] = new FeedExtension("FeedTest", "Feed Test Extension");

            // All done
            this.onLoadFinishedHandler(this.currentWorkspace);
        } catch (error) {
            console.error(error);
            this.onLoadError(error.message);
        }
    }

    // Grabs details from config
    public getConfigValue(key: string, defaultValue : any, scope : WorkspaceConfigScope): any {
        // Same as above but as a switch statement
        switch (scope) {
            case WorkspaceConfigScope.Default:
                return Teyora.TY.PreferencesManager.getPreference(
                    `workspaceConfigOverride/${this.currentWorkspace.meta.teyoraWorkspaceID}/${key}`,
                    Teyora.TY.PreferencesManager.getPreference(`workspaceConfigGlobal/${key}`, (
                        this.currentWorkspace.workspaceConfig[key] || defaultValue
                    )));
            case WorkspaceConfigScope.Workspace:
                return this.currentWorkspace.workspaceConfig[key] || defaultValue;
            case WorkspaceConfigScope.UserGlobal:
                return Teyora.TY.PreferencesManager.getPreference(
                    `workspaceConfigGlobal/${this.currentWorkspace.meta.teyoraWorkspaceID}/${key}`,
                    defaultValue
                );
            case WorkspaceConfigScope.UserWorkspace:
                return Teyora.TY.PreferencesManager.getPreference(
                    `workspaceConfigOverride/${this.currentWorkspace.meta.teyoraWorkspaceID}/${key}`,
                    defaultValue
                );
            default:
                console.warn("Warning: You are calling getConfigValue with an invalid scope. The default value will be returned.");
                return defaultValue;
        }
    }

}