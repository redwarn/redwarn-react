// This is for moderating use of Teyora, not MediaWiki.
import { ExitToApp, Web, Add, Search, Key } from "@mui/icons-material";
import { Avatar, Box, Button, Container, Grid, IconButton, List, ListItem, ListItemAvatar, ListItemButton, ListItemIcon, ListItemText, Paper, Stack, Tooltip, Typography } from "@mui/material";
import * as React from "react";
import { withTranslation } from "react-i18next";
import Teyora, { ITYWindowProps } from "../../App";
import WorkspaceSelectorAppBar from "../workspace/WorkspaceSelectorAppBar";

class AdminHome extends React.Component<ITYWindowProps> {
    render() {
        if (!Teyora.TY.CurrentUser.isAdmin) throw new Error("You do not have permission to access this page.");
        Teyora.TY.LinkHandler.setURL("admin", []);

        const { t } = this.props; // These are filled in by the withTranslation() decorator.
        return (
            <Box
                sx={{
                    margin: 0,
                    padding: 0,
                    bgcolor: "background.default",
                    color: "text.primary",
                }}
            >
                <WorkspaceSelectorAppBar /> {/* We can reuse this */}
                <Container maxWidth="lg" sx={{paddingTop: "2vh"}}>
                    <Stack spacing={1}>
                        <Typography variant="h2">
                            {t("ui:admin.title")}
                        </Typography>
                        <Typography variant="h5">
                            {t("ui:admin.subtitle")}
                        </Typography>
                        <Grid spacing={2} container>
                            <Grid item xs={8}>
                                <Paper>
                                    <Box p={2}>
                                        <Typography variant="h6">
                                            {t("ui:admin.tools")}
                                        </Typography>

                                        
                                        <List sx={{ width: "100%" }}>       
                                            <ListItem
                                                secondaryAction={
                                                    <Stack spacing={1} direction="row">
                                                        <Button variant="outlined" onClick={()=>{
                                                            // Open the workspace
                                                            alert("Todo");
                                                        }}>
                                                            {t("ui:workspaceSelector.openWorkspace")}
                                                        </Button>
                                                        {/* The leave button also acts as a transfer/delete for the owner of a workspace */}
                                                        <Tooltip title={t("ui:workspaceSelector.leaveWorkspace")}>
                                                            <IconButton>
                                                                <ExitToApp />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Stack>
                                                }>
                                                <ListItemAvatar>
                                                    <Avatar>
                                                        <Web />
                                                    </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText primary={"Modify User"} secondary={
                                                    "Test"
                                                } />
                                            </ListItem>
                                        </List>
                                            
                                    </Box>
                                </Paper>
                            </Grid>

                            {/* Actions: Edit, create, join, etc. */}
                            <Grid item xs={4}>
                                <Stack spacing={2} direction="column">
                                    <Paper>
                                        <Box p={2}>
                                            <Typography variant="h6">
                                                {t("ui:workspaceSelector.getWorkspaces")}
                                            </Typography>
                                            <List>
                                                <ListItem disablePadding>
                                                    <ListItemButton onClick={()=>{
                                                    // Creation is only available to approved users
                                                    // todo: stop creation if more than 100 workspaces joined
                                                        alert("todo");
                                                    }}>
                                                        <ListItemIcon>
                                                            <Add />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.createNewWorkspace")} />
                                                    </ListItemButton>
                                                </ListItem>
                                                <ListItem disablePadding>
                                                    <ListItemButton>
                                                        <ListItemIcon>
                                                            <Search />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.searchStoreWorkspaces")} />
                                                    </ListItemButton>
                                                </ListItem>
                                                <ListItem disablePadding>
                                                    <ListItemButton>
                                                        <ListItemIcon>
                                                            <Key />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.joinPrivate")} />
                                                    </ListItemButton>
                                                </ListItem>
                                            </List>
                                        </Box>
                                    </Paper>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </Container>
            </Box>
        );
    }
}
export default withTranslation()(AdminHome);