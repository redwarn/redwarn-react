import * as React from "react";
import { AppBar, Box, Toolbar } from "@mui/material";
import { TYWordmarkPrimaryContrasted } from "../components/TYLogo";
import AppbarUserMenu from "../components/AppbarUserMenu";


export default function WorkspaceSelectorAppBar() {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" enableColorOnDark>
                <Toolbar>
                    {/* Logo // Todo: On click send back to home */}
                    <Box sx={{ flexGrow: 1 }}>
                        <TYWordmarkPrimaryContrasted style={{
                            height: "40px",
                            width: "auto",
                            paddingTop: "6px"
                        }} />
                    </Box>
                    
                    {/* Avatar button and menu */}
                    <Box sx={{ flexGrow: 0 }}>
                        <AppbarUserMenu />
                    </Box>
                    
                </Toolbar>
            </AppBar>
        </Box>
    );
            
}
