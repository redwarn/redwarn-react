import { Alert, Box, Button, Container, IconButton, Popover, Stack, Tooltip, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import TeyoraUI from "../../../TeyoraUI";
import Teyora from "../../../../App";
import { TeyoraPatrol } from "../TeyoraPatrol";
import { DateTime } from "luxon";
import { ExpandLess, ExpandMore, Settings, ExitToApp } from "@mui/icons-material";
import { LocalisedButtons } from "../../../LocalisedButtons";

export interface JobsDrawerContentProps {
    isInFooterMode: boolean; // Whether the drawer is acting as a footer
}

export default function JobsDrawerContent(props: JobsDrawerContentProps) {
    const [ jobHasFailed, setJobHasFailed ] = React.useState(false); // Testing

    // For clocks
    const [ currentTimeUTC, setCurrentTimeUTC ] = React.useState(DateTime.utc().toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS));
    const [ currentTimeUser, setCurrentTimeUser ] = React.useState(DateTime.local().toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS));

    // Update clock every second
    React.useEffect(() => {
        const interval = setInterval(() => {
            setCurrentTimeUTC(DateTime.utc().toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS));
            setCurrentTimeUser(DateTime.local().toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS));
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    const { t } = useTranslation();
    const lB = LocalisedButtons();
    const jobFailedPoperOverRef = React.useRef(); // Used for "Job failed" popover

    return (
        <>
            {/* Visible in all modes, acts as header */}
            <Box sx={{
                height: "2rem",
                width: "100%",
                display: "flex",
                alignItems: "center",
            }}>
                {/* Left-aligned */}
                <Box sx={{
                    display: "flex",
                    flexGrow: 1,
                }}>
                    <Stack spacing={1} direction="row" sx={{
                        paddingLeft: "1rem",
                    }}>
                        <Typography variant="body2" sx={{
                            fontVariantCaps: "all-small-caps",
                        }}>
                            {t("patrol:jobs.status")}
                        </Typography>
                        <Typography variant="body2" sx={{
                            fontVariantCaps: "all-small-caps",
                            color: "warning.light"
                        }}>
                            {t("patrol:jobs.pending", { count: 0 })}
                        </Typography>
                        <Typography variant="body2" sx={{
                            fontVariantCaps: "all-small-caps",
                            color: "info.dark"
                        }}>
                            {t("patrol:jobs.inProgress", {count : 0})}
                        </Typography>
                        <Typography variant="body2" sx={{
                            fontVariantCaps: "all-small-caps",
                            color: "success.dark"
                        }}>
                            {t("patrol:jobs.completed", {count : 0})}
                        </Typography>
                        <Typography variant="body2" ref={jobFailedPoperOverRef} sx={{
                            fontVariantCaps: "all-small-caps",
                            color: "error.dark"
                        }}>
                            {t("patrol:jobs.failed", {count : 0})}
                        </Typography>

                        {/* Popover for failed jobs in Footer mode only, will appear top right */}
                        <Popover
                            open={jobHasFailed && props.isInFooterMode}
                            anchorEl={jobFailedPoperOverRef.current}
                            anchorOrigin={{
                                vertical: "top",
                                horizontal: "left",
                            }}
                        >
                            <Container sx={{ p: 2, width: "40vh" }}>
                                <Stack spacing={1}>
                                    <Typography variant="h6" sx={{
                                        color: "error.dark",
                                    }}>
                                        {t("patrol:jobs.failedPopover.title")}
                                    </Typography>
                                    <Typography variant="body1">
                                        {t("patrol:jobs.failedPopover.description")}
                                    </Typography>
                                    <Button variant="contained" onClick={()=>{
                                        TeyoraPatrol.TP.toggleJobsDrawerFooterMode();
                                    }}>
                                        {t("patrol:jobs.failedPopover.seeDetailsButton")}
                                    </Button>

                                    <Button variant="outlined" onClick={()=>{
                                        setJobHasFailed(false);
                                    }}>
                                        {t("patrol:jobs.failedPopover.dismissButton")}
                                    </Button>

                                    <Button variant="text" color={"error"} onClick={()=>{
                                        TeyoraUI.showAlertBox("TODO: Implement this");
                                    }}>
                                        {t("patrol:jobs.failedPopover.cancelAllFailedButton")}
                                    </Button>
                                </Stack>
                            </Container>
                        </Popover>
                    </Stack>
                </Box>

                {/* Right-aligned */}
                <Box sx={{
                    display: "flex",
                    flexGrow: 0,
                    justifyContent: "flex-end",
                    paddingRight: "1rem",
                }}>
                    <Stack spacing={1} direction="row">
                        {/* Text is boxed seperately to stop them being pushed up by icons */}

                        

                        {/* Workspace Name */}
                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                            alignContent: "center",
                        }}>
                            <Tooltip title={t("patrol:jobs.workspaceNameTooltip")}>
                                <Typography variant="body2" sx={{
                                    fontVariantCaps: "all-small-caps",
                                }}>
                                    { Teyora.TY.WorkspaceManager.currentWorkspace.meta.workspaceName }
                                </Typography>
                            </Tooltip>
                        </Box>

                        {/* Current Wiki */}
                        <Box sx={{ display: "flex", alignContent: "center", alignItems: "center" }}>
                            <Tooltip title={t("patrol:currentWiki")}>
                                <Typography variant="body2" sx={{
                                    fontVariantCaps: "all-small-caps",
                                    color: "info.dark",
                                }}>
                                    {Teyora.TY.CurrentUser.username.split(":")[1]}@wikihere
                                </Typography>
                            </Tooltip>
                        </Box>

                        {/* Clock */}
                        <Box sx={{ display: "flex", alignContent: "center", alignItems: "center" }}>
                            <Tooltip title={t("patrol:currentTimeUTC")}>
                                <Typography variant="body2" sx={{
                                    fontVariantCaps: "all-small-caps",
                                    color: "info.light",
                                }}>
                                    { currentTimeUTC }
                                </Typography>
                            </Tooltip>
                        </Box>

                        {/* User timezone - when clicked show timer popover */}
                        <Box sx={{ display: "flex", alignContent: "center", alignItems: "center", cursor: "pointer" }}
                            onClick={
                                ()=>{
                                    TeyoraUI.showAlertBox("Somehow this is not implemented yet... but when it is you'll be able to set rest timers");
                                }
                            }>
                            <Tooltip title={t("patrol:currentTimeUser")}>
                                <Typography variant="body2" sx={{
                                    fontVariantCaps: "all-small-caps",
                                    color: "info.dark",
                                }}>
                                    { currentTimeUser }
                                </Typography>
                            </Tooltip>
                        </Box>

                        {/* Expand/collapse button */}
                        <Tooltip title={props.isInFooterMode ? t("patrol:jobs.moreInfo") : t("patrol:jobs.lessInfo")}>
                            <IconButton size={"small"} onClick={()=>{
                                TeyoraPatrol.TP.toggleJobsDrawerFooterMode();
                            }}>
                                { props.isInFooterMode ? <ExpandMore fontSize={"small"} /> : <ExpandLess fontSize={"small"} /> }
                            </IconButton>
                        </Tooltip>

                        {/* Workspace Settings button */}
                        <Tooltip title={t("patrol:workspaceSettings")}>
                            <IconButton size={"small"} onClick={()=>{
                                TeyoraPatrol.TP.openWorkspaceSettings();
                            }}>
                                <Settings fontSize={"small"} />
                            </IconButton>
                        </Tooltip>

                        {/* Exit workspace button */}
                        <Tooltip title={t("patrol:exitWorkspace")}>
                            <IconButton size={"small"} onClick={async ()=>{
                                const confirmExit = await TeyoraUI.showAlertBox(t("patrol:exitWorkspaceConfirm"), t("patrol:exitWorkspaceConfirmTitle"), [
                                    lB.NO,
                                    lB.YES,
                                ]);
                                if (confirmExit == lB.YES) window.location.href = "/";
                            }}>
                                <ExitToApp fontSize={"small"} />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Box>
            </Box>

            {/* Visible when not in footer mode */}
            {!props.isInFooterMode && (<Box sx={{
                height: "100%",
                width: "100%",
                overflowY: "auto",
                paddingLeft: "1rem",
                paddingRight: "1rem",
            }}>
                {/* Alert on failed jobs */}
                {jobHasFailed && <Alert variant="outlined" severity="error" onClose={() => {
                    setJobHasFailed(false);
                }}>{t("patrol:jobs.failedAlert")}</Alert>}

                {/* Job list // TODO //*/}
                <Typography variant="h6">
                    There will be a job list here once thats working :)
                </Typography>
            </Box>)}
        </>
    );
}