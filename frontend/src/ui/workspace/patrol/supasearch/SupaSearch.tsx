/*
Supasearch
Like Spotlight but for Wikimedia projects

Each section is handled by a "search handler" which searches each wiki, setting, etc.
*/

import * as React from "react";
import { Alert, CircularProgress, Container, Dialog, Stack, TextField, Typography } from "@mui/material";
import { withTranslation } from "react-i18next";
import { ITYWindowProps } from "../../../../App";
import i18n from "../../../../i18n";
import TeyoraUI from "../../../TeyoraUI";
import MediaWikiHanlder, { MediawikiSearchResults } from "./handlers/MediaWiki";
import MWResultComponent from "./MWResultComponent";
import { TeyoraPatrol } from "../TeyoraPatrol";
import { ErrorBoundary } from "react-error-boundary";

interface SupaSearchProps extends ITYWindowProps {
    open: boolean;
    onClose: () => void;
}

interface SupaSearchState {
    searchQuery: string;
    currentMWSearchResults: MediawikiSearchResults;
    mwSearchInProgress: boolean;
}

class SupaSearch extends React.Component<SupaSearchProps, SupaSearchState> {
    private searchActionTimeout: NodeJS.Timeout;

    constructor(props: SupaSearchProps) {
        super(props);
        this.state = {
            searchQuery: "",
            currentMWSearchResults: {},
            mwSearchInProgress: false,
        };
    }

    private makeMediaWikiSearch = async (searchQuery: string) => {
        const { t } = i18n;
        try {
            this.setState({ mwSearchInProgress: true });
            const results = await MediaWikiHanlder(searchQuery);
            // TODO: Add result sorting here based on active wiki + number of search results
            this.setState({ currentMWSearchResults: results, mwSearchInProgress: false });
        } catch (error) {
            console.error(error);
            this.setState({ mwSearchInProgress: false });

            // Send a toast
            TeyoraUI.showSnackbar(t("patrol:supasearch.MWSearchError", { "error": error.message }), {
                variant: "error",
            });
        }
    };

    render() {
        const { props, state } = this;
        const { t } = props; // Don't import from i18n here! Use the one passed into our props

        return <Dialog open={props.open} onClose={props.onClose} maxWidth={false}>
            <Container sx={{
                width: "50vw",
                paddingTop: "1vh",
                paddingBottom: "1vh"
            }}>
                <TextField label={t("patrol:supasearch.search")} sx={{
                    fontSize: "2rem",
                    width: "100%"
                }}
                value={state.searchQuery} onChange={(e) => {
                    const newText = e.target.value;
                    this.setState({
                        searchQuery: newText,
                    });

                    // No point in searching if there's no query
                    if (newText.length === 0 || !newText) {
                        // Clear the results and timeout
                        if (this.searchActionTimeout) clearTimeout(this.searchActionTimeout);
                        this.setState({ currentMWSearchResults: {} });
                        return;
                    }

                    // We wait 750ms before doing a search
                    if (this.searchActionTimeout) {
                        clearTimeout(this.searchActionTimeout);
                    }
                    this.searchActionTimeout = setTimeout(async () => {
                        // TODO: We need to do something to prevent race conditions
                        await this.makeMediaWikiSearch(newText);
                    }, 750);
                }} />

                {/* MW Search progress indicatior */}
                {state.mwSearchInProgress && <Stack direction={"row"} spacing={"2rem"}>
                    <CircularProgress />
                    <Typography variant="body1">
                        {t("patrol:supasearch.searchingMW")}
                    </Typography>
                </Stack>}

                {/* MW Search results. We error boundary this in case something goes wrong. */}
                <ErrorBoundary key={state.searchQuery} FallbackComponent={e => <div>
                    <Alert severity="error">
                        {t("patrol:supasearch.MWSearchError", {error: e.error.message})}
                    </Alert>
                </div>}>
                    {Object.keys(state.currentMWSearchResults).length > 0 && <Stack spacing={"2rem"}>
                        {Object.keys(state.currentMWSearchResults).map((wiki) => {
                            return <MWResultComponent
                                wikiID={wiki}
                                results={state.currentMWSearchResults[wiki]}
                                key={wiki}
                                onSelect={(result) => {
                                    props.onClose();
                                    TeyoraPatrol.TP.openWikiPage(wiki, result.page, true);
                                }}
                            />;
                        })}
                    </Stack>}
                </ErrorBoundary>

            </Container>
        </Dialog>;
    }
}

export default withTranslation()(SupaSearch);