/*
Search MediaWiki for the requested page.
This uses the SupaSearch API endpoint and only searches for Wikis actually
added to the current workspace.
*/

import Teyora from "../../../../../App";
import i18n from "../../../../../i18n";
import { WorkspaceConfigScope } from "../../../../../manager/WorkspaceManager";

export type MediawikiSearchResult = {
    pID: number;
    page: string;
    snippet: string;
    timestamp: string;
}

export type MediawikiSearchResults = Record<string, Record<string, MediawikiSearchResult[]>>;

export default async function MediaWikiHandler(searchQuery: string) : Promise<MediawikiSearchResults> {
    const { t } = i18n; // For error messages

    // If searchQuery is empty, throw an error.
    if (searchQuery.length === 0) {
        throw new Error(t("patrol:supasearch.emptySearchQuery"));
    }

    // If searchQuery is too long (over 100 chars), throw an error.
    if (searchQuery.length > 100) {
        throw new Error(t("patrol:supasearch.queryTooLong"));
    }

    // If there are no wikis in the current workspace
    if (Teyora.TY.WorkspaceManager.getConfigValue("wikis", [], WorkspaceConfigScope.Workspace).length === 0) {
        throw new Error(t("patrol:supasearch.noWikisInWorkspace"));
    }

    // Get the current workspace ID... it's a bit deeply nested
    const workspaceID = Teyora.TY.WorkspaceManager.currentWorkspace.meta.teyoraWorkspaceID;
    // Make a PUT request to the SupaSearch API endpoint
    const response = await fetch("/api/workspace/wiki/supasearch", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            workspaceID,
            searchQuery,
        }),
    });

    // If the response is unsuccessful, throw an error
    if (!response.ok) throw new Error(response.statusText);
    
    // Parse the response to JSON
    const responseJSON = await response.json();

    return responseJSON.wikiSearchResults;
}