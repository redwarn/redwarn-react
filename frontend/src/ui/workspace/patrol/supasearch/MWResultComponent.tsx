import { ExpandMore } from "@mui/icons-material";
import { Typography, Accordion, AccordionSummary, AccordionDetails } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../../../App";
import { MediawikiSearchResult } from "./handlers/MediaWiki";
import MWResultContent from "./MWResultContent";

export default function MWResultComponent(props: { wikiID: string, results: Record<
    string,
    // eslint-disable-next-line no-unused-vars
    MediawikiSearchResult[]>, onSelect: (result: MediawikiSearchResult) => void }) {
    // Set up the relevant information for the result
    const { t } = useTranslation();
    const { wikiID, results } = props;
    const wikiSupport = Teyora.TY.WikiSupport[wikiID];

    const resultsOrder = Object.keys(results).sort((a, b) => {
        // Sort by key with highest number of results
        const aCount = results[a].length;
        const bCount = results[b].length;
        if (aCount > bCount) {
            return -1;
        } else if (aCount < bCount) {
            return 1;
        } else {
            return 0;
        }
    });

    return (
        <Box>
            <Typography variant="h6">{(wikiSupport.langName !== "Special") && wikiSupport.langName} {wikiSupport.name}</Typography>
            {/* Map the resultsorder to an accordian. TODO: Extract this into its own component */}
            {resultsOrder.length > 0 ? resultsOrder.map((ns, i) => {
                return (
                    <MWResultContent key={i} wikiID={wikiID} ns={ns.toString()} i={i} results={results[ns]} onSelect={
                        (result: MediawikiSearchResult) => {
                            props.onSelect(result);
                        }
                    } />
                );
            }) :
                // No results
                <Typography variant="body1" sx={{
                    color: "text.secondary",
                    fontStyle: "italic",
                    textAlign: "center",
                }}>{t("patrol:supasearch.noresultsmw")}</Typography>}
        </Box>
    );
}