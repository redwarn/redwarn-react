import { FormatListBulleted, Pause, PlayArrow, Settings, ViewList } from "@mui/icons-material";
import { Paper, Typography, Stack, Box, IconButton, Tooltip, ButtonBase, Collapse, CircularProgress } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import i18n from "../../../../i18n";
import TeyoraUI, { TeyoraUISounds } from "../../../TeyoraUI";
import FeedEdit from "./FeedEdit";
import { DateTime } from "luxon";
import { TeyoraPatrol } from "../TeyoraPatrol";
import { TransitionGroup } from "react-transition-group";
import Teyora from "../../../../App";

export interface iFeedWarning {
    type: "info" | "warning" | "error";
    message: string;
    timestamp: DateTime;
}
export interface iFeed {
    id: string;
    launchMessages: iFeedWarning[];
    feedExtensionID: string; // Once loaded we actually modify this and call init and subscribe with our wikis
    wikis: string[];
    filters: iFeedFilter[]; // Changed to filter type
    title: string;
    maxLength: number;
    soundOnNewEdit?: TeyoraUISounds;
}

export interface iFeedFilter {
    id: string;
    name: string;
    type: "tag" | "hide",
    tagColour?: string,
    tag?: string,
}

interface FeedEntryProps {
    condensed: boolean;
    edit: FeedEdit;
}

function ExplainORESScores() {
    const { t } = i18n; // Don't use useTranslation() here, this isn't a component
    TeyoraUI.showAlertBox(t("patrol:feeddrawer.oresExplainer.description"), t("patrol:feeddrawer.oresExplainer.title"));
}

export function FeedEntry(props: FeedEntryProps) {
    const { t } = useTranslation();
    const [timeSinceEdit, setTimeSinceEdit] = React.useState(DateTime.fromJSDate(props.edit.timestamp).toRelative());
    const [mouseOver, setMouseOver] = React.useState(false);

    // Update how long ago the edit was every second
    React.useEffect(() => {
        const interval = setInterval(() => {
            setTimeSinceEdit(DateTime.fromJSDate(props.edit.timestamp).toRelative());
        }, 1000);
        return () => clearInterval(interval);
    }, [props.edit.timestamp]);


    return props.condensed ? (
        // Condensed
        <Paper elevation={!mouseOver ? 1 : 4} square sx={{
            cursor: "pointer",
        }}
        onMouseEnter={() => setMouseOver(true)}
        onMouseLeave={() => setMouseOver(false)}>
            <Stack sx={{
                paddingLeft: "1rem",
            }}>
                {/* Revision tag and page title */}
                <Stack direction={"row"} spacing={1}>
                    {
                        props.edit.tagColour && (
                            <Tooltip title={props.edit.tagName || t("patrol:feeddrawer.edit.tagTooltip")}>
                                <Typography variant="body1" sx={{
                                    color: props.edit.tagColour,
                                    cursor: "help",
                                }}>
                                    ⬤ {/* Just use unicode here */}
                                </Typography>
                            </Tooltip>
                        )
                    }
                    <Typography variant="body1" noWrap>
                        {props.edit.pageTitle}
                    </Typography>
                </Stack>

                {/* Bottom row */}
                <Stack spacing={1} direction={"row"}>
                    {/* User */}
                    <Typography variant="caption" sx={{
                        width: "10rem",
                        maxWidth: "10rem",
                    }} noWrap>
                        {props.edit.user}
                    </Typography>
                    {/* Change size */}
                    <Typography variant="caption" noWrap sx={{
                        color: props.edit.diff > 0 ? "success.dark" :
                            props.edit.diff < 0 ? "error.dark" :
                                "palette.text.primary",
                    }}>
                        {props.edit.diff > 0 && "+"}
                        {props.edit.diff}
                    </Typography>

                    {/* ORES (if present) */}
                    {props.edit.oresGoodFaith != null && (
                        <Tooltip title={t("patrol:feeddrawer.oresGoodFaithTooltip")}>
                            <ButtonBase onClick={()=>ExplainORESScores()} sx={{ cursor : "help" }}>
                                <Typography variant="caption" noWrap sx={{
                                    // Red to green is hues 0 to 128
                                    color: `hsl(${props.edit.oresGoodFaith * 128},100%,40%);`
                                }}>
                                    GF: { Number(props.edit.oresGoodFaith).toLocaleString(undefined,{style: "percent", maximumFractionDigits:0})  }
                                </Typography>
                            </ButtonBase>
                        </Tooltip>
                    )}

                    {props.edit.oresDamaging != null && (
                        <Tooltip title={t("patrol:feeddrawer.oresDamagingTooltip")}>
                            <ButtonBase onClick={()=>ExplainORESScores()} sx={{ cursor : "help" }}>
                                <Typography variant="caption" noWrap sx={{
                                    // Reverse it here as we want 100% to be red
                                    color: `hsl(${(1 - props.edit.oresDamaging) * 128},100%,40%);`
                                }}>
                                    DM: { Number(props.edit.oresDamaging).toLocaleString(undefined,{style: "percent", maximumFractionDigits:0})  }
                                </Typography>
                            </ButtonBase>
                        </Tooltip>
                    )}

                </Stack>
            </Stack>
        </Paper>
    ) : (
        // Normal view
        <Paper elevation={!mouseOver ? 1 : 4} square sx={{
            cursor: "pointer",
        }}
        onMouseEnter={() => setMouseOver(true)}
        onMouseLeave={() => setMouseOver(false)}
        >
            <Stack sx={{
                paddingLeft: "1rem",
                paddingRight: "1rem",
                paddingTop: "0.5rem",
                paddingBottom: "0.5rem",
            }}>
                {/* First row, Page title */}
                <Typography variant="body1" noWrap sx={{
                    fontSize: "1.25rem",
                }}>
                    {props.edit.pageTitle}
                </Typography>

                {/* Second row, User */}
                <Stack direction={"row"} spacing={1}>
                    <Typography variant="body1" noWrap sx={{
                        fontWeight: "bold",
                        width: "15rem",
                    }}>
                        {props.edit.user}
                    </Typography>

                    {/* Edit diff size */}
                    <Typography variant="body1" noWrap sx={{
                        color: props.edit.diff > 0 ? "success.dark" :
                            props.edit.diff < 0 ? "error.dark" :
                                "palette.text.primary",
                    }}>
                        {props.edit.diff > 0 && "+"}
                        {props.edit.diff.toLocaleString()}
                    </Typography>
                </Stack>
                
                {/* Edit summary */}
                <Typography variant="body1" noWrap sx={{
                    fontStyle: "italic",
                    fontSize: "0.875rem",
                    color: props.edit.summary ? "palette.text.secondary" : "info.light",
                }}>
                    {props.edit.summary || t("patrol:feeddrawer.noEditSummary")}
                </Typography>


                {/* Tags / ores */}
                <Stack direction={"row"} spacing={1}>
                    {/* Revision tag (if present) */}
                    {props.edit.tagColour && (
                        <Typography variant="caption" sx={{
                            color: props.edit.tagColour,
                            maxWidth: "10rem",
                        }} noWrap>
                            {props.edit.tagName || t("patrol:feeddrawer.edit.tagTooltip")}
                        </Typography>
                    )}

                    {/* New page tag (if present) */}
                    {props.edit.isNewPage && (
                        <Tooltip title={t("patrol:feeddrawer.edit.newPageTooltip")}>
                            <Typography variant="caption" sx={{
                                color: "success.dark",
                            }} noWrap>
                                {t("patrol:feeddrawer.edit.newPage")}
                            </Typography>
                        </Tooltip>
                    )}

                    {/* Minor edit (if present) */}
                    {props.edit.minor && (
                        <Tooltip title={t("patrol:minorEdit.shortHelper")}>
                            <Typography variant="caption" noWrap sx={{
                                color: "info.main",
                                fontVariantCaps: "all-small-caps",
                                cursor: "help",
                            }}>
                                {t("patrol:minorEdit.short")}
                            </Typography>
                        </Tooltip>
                    )}

                    {/* Bot edit (if present) */}
                    {props.edit.bot && (
                        <Tooltip title={t("patrol:botEdit.shortHelper")}>
                            <Typography variant="caption" noWrap sx={{
                                color: "info.main",
                                fontVariantCaps: "all-small-caps",
                                cursor: "help",
                            }}>
                                {t("patrol:botEdit.short")}
                            </Typography>
                        </Tooltip>
                    )}

                    {/* User edit count (if present) */}
                    {props.edit.userEditCount != null && (
                        <Tooltip title={t("patrol:userEditCount.shortHelper")}>
                            <Typography variant="caption" noWrap sx={{
                                color: "info.main",
                                cursor: "help",
                            }}>
                                {props.edit.userEditCount.toLocaleString()}
                            </Typography>
                        </Tooltip>
                    )}

                    {/* ORES (if present) */}
                    {props.edit.oresGoodFaith != null && (
                        <Tooltip title={t("patrol:feeddrawer.oresGoodFaithTooltip")}>
                            <ButtonBase onClick={()=>ExplainORESScores()} sx={{ cursor : "help" }}>
                                <Typography variant="caption" noWrap sx={{
                                    // Red to green is hues 0 to 128
                                    color: `hsl(${props.edit.oresGoodFaith * 128},100%,40%);`
                                }}>
                                    GF: { Number(props.edit.oresGoodFaith).toLocaleString(undefined,{style: "percent", maximumFractionDigits:0})  }
                                </Typography>
                            </ButtonBase>
                        </Tooltip>
                    )}

                    {props.edit.oresDamaging != null && (
                        <Tooltip title={t("patrol:feeddrawer.oresDamagingTooltip")}>
                            <ButtonBase onClick={()=>ExplainORESScores()} sx={{ cursor : "help" }}>
                                <Typography variant="caption" noWrap sx={{
                                    // Reverse it here as we want 100% to be red
                                    color: `hsl(${(1 - props.edit.oresDamaging) * 128},100%,40%);`
                                }}>
                                    DM: { Number(props.edit.oresDamaging).toLocaleString(undefined,{style: "percent", maximumFractionDigits:0})  }
                                </Typography>
                            </ButtonBase>
                        </Tooltip>
                    )}
                </Stack>

                {/* Lastly, the time */}
                <Typography variant="caption" noWrap>
                    {timeSinceEdit}
                </Typography>
            </Stack>
        </Paper>
    );
}

export default function Feed(props: iFeed) {
    const [isLoading, setIsLoading] = React.useState(true);
    const [feedCrashed, setFeedCrashed] = React.useState(false);
    const [warnings, setWarnings] = React.useState(props.launchMessages);

    const [isPaused, setPaused] = React.useState(false); 
    const [compactMode, setCompactMode] = React.useState(false);

    const [currentFeed, setCurrentFeed] = React.useState([] as FeedEdit[]);

    React.useEffect(()=>{
        // Find our feed extension and launch it
        const feedExtension = Teyora.TY.WorkspaceManager.FeedExtensions[props.feedExtensionID];
        const onFail = ()=>{
            setIsLoading(false);
            setFeedCrashed(true);
        };
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        let unSub = ()=>{};

        (async ()=>{
            if (!feedExtension.hasInit) {
                try {
                    await feedExtension.init();
                    setIsLoading(false);
                } catch {
                    setFeedCrashed(true);
                    return;
                }
            }

            // Add us to the feed extension listeners
            feedExtension.extensionFailListeners.push(onFail);
            unSub = feedExtension.subscribe(props.wikis, (feedEdit: FeedEdit)=>{
                if (isPaused) return;
                // If sound set, play it
                if (props.soundOnNewEdit) TeyoraUI.playSound(props.soundOnNewEdit);
                // Shorten array to limitand add the edit
                setCurrentFeed([feedEdit, ...currentFeed.splice(0, (currentFeed.length > props.maxLength ? props.maxLength : currentFeed.length))]);
            });
        })();

        return ()=>{
            // Remove us from the feed extension listeners
            feedExtension.extensionFailListeners = feedExtension.extensionFailListeners.filter(l=>l!==onFail);
            unSub();
        };
    }, [props.feedExtensionID, props.wikis, props.maxLength, isPaused, currentFeed]);


    const { t } = useTranslation();
    return <Paper variant="outlined" square>
        <Stack spacing={1} direction={"row"}>
            <Box sx={{
                flexGrow: 1,
                display: "flex",
                alignItems: "center",
            }}
            >
                
                <Typography variant="body2" sx={{
                    paddingLeft: "1rem",
                    fontVariantCaps: "all-small-caps",
                    letterSpacing: "0.1em",
                    fontSize: "1rem",
                }} noWrap>
                    {props.title}
                    {isPaused && t("patrol:feeddrawer.feed.titlePaused")}
                    {isLoading && t("patrol:feeddrawer.feed.titleLoading")}
                </Typography>
                
            </Box>
            <Box sx={{
                flexGrow: 0,
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center",
            }}
            >
                <Stack spacing={1} direction={"row"}>
                    {!isLoading && !feedCrashed ? (
                        <>
                            {/* Live updates play/pause */}
                            <Tooltip title={ isPaused? t("patrol:feeddrawer.tooltip.play") : t("patrol:feeddrawer.tooltip.pause") }>
                                <IconButton size={"small"} onClick={()=>setPaused(!isPaused)}>
                                    {isPaused ? <PlayArrow /> : <Pause />}
                                </IconButton>
                            </Tooltip>

                            {/* Compact mode */}
                            <Tooltip title={ compactMode? t("patrol:feeddrawer.tooltip.expand") : t("patrol:feeddrawer.tooltip.compact") }>
                                <IconButton size={"small"} onClick={()=>setCompactMode(!compactMode)}>
                                    {compactMode ? <ViewList fontSize={"small"} /> : <FormatListBulleted fontSize={"small"} />}
                                </IconButton>
                            </Tooltip>
                        </>
                    ) : ( isLoading && <Box sx={{ display: "flex", alignItems: "center" }}>
                        <CircularProgress size={15} />
                    </Box> )}

                    {/* Settings */}
                    <Tooltip title={ t("patrol:feeddrawer.tooltip.settings") }>
                        <IconButton size={"small"}>
                            <Settings fontSize={"small"} />
                        </IconButton>
                    </Tooltip>
                </Stack>
            </Box>
        </Stack>
        {!isLoading && !feedCrashed ? <Stack spacing={0.25} direction={"column"} sx={{
            overflowY: "auto",
        }}>
            <TransitionGroup>
                {currentFeed.map((entry) => (
                    <Collapse key={entry.id}>
                        <FeedEntry condensed={compactMode} edit={entry} key={entry.id} />
                    </Collapse>
                ))}
            </TransitionGroup>
            
        </Stack> : (
            <Box sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexGrow: 1,
            }}>
                <Typography variant="body2" color={feedCrashed ? "error.dark" : "text.secondary"} >
                    {feedCrashed ? t("patrol:feeddrawer.feed.crashed") :
                        t("patrol:feeddrawer.feed.loading")}
                </Typography>
            </Box>
        )}
    </Paper>;
}