/* eslint-disable semi */
// Interface for edit information that is shown in a feed entry
// This is also used as a state
export default interface FeedEdit {
    // The edit ID /
    id: string;
    // The WikiID of the wiki that this edit is for
    wikiId: string;
    // The page title of the page that this edit is for
    pageTitle: string;
    // The user that made this edit
    user: string;

    // The edit count of the user
    userEditCount?: number;

    // The edit summary
    summary: string;

    // Edit marked as minor
    minor?: boolean;

    // Edit marked as bot
    bot?: boolean;


    // If the page is new
    isNewPage?: boolean;

    // The timestamp of when this edit was made
    timestamp: Date; // for "10 seconds ago" etc.

    // Byte difference
    diff: number;

    // ORES information about this edit
    oresGoodFaith?: number; 
    oresDamaging?: number;

    // Tag info (if flagged with colour)
    tagColour?: string;
    tagName?: string;

}