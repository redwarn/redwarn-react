import { People } from "@mui/icons-material";
import { Avatar, Button, Container, Link, List, ListItem, ListItemAvatar, ListItemText, Stack, Typography } from "@mui/material";
import * as React from "react";
import { withTranslation } from "react-i18next";
import { TY_FRONTEND_VERSION } from "../../../../../../common/src/Constants";
import Teyora from "../../../../App";
import TeyoraUI, { TeyoraUISoundRecord, TeyoraUISounds } from "../../../TeyoraUI";
import { StateFragment } from "@hookstate/core";
import { TeyoraPatrol } from "../TeyoraPatrol";
import { RequiredTabProps, TPTab } from "../baseclasses/Tab";

interface HomepagePState {
    // Test count
    testCount: number;
}

interface HomepageRState {
    // Test count
    testCount: number;
}
class Homepage extends TPTab<HomepagePState, RequiredTabProps, HomepageRState> {
    constructor(props: RequiredTabProps) {
        // Set our base state
        super(props, {
            testCount: 0,
        });

        // Temp state
        this.state = {
            testCount: 0,
        };
    }

    render() {
        const { t } = this.props;
        const testState = this.getPState("testCount");
        const testRState = this.state.testCount;

        return <Container sx={{
            paddingTop: "1rem",
        }}>
            <Typography variant="h4">
                {t("patrol:home.title", { version: TY_FRONTEND_VERSION, user: TeyoraUI.getUserRef() })}
            </Typography>
            <Typography variant="h6">
                {t("patrol:home.subtitle", { workspaceName: Teyora.TY.WorkspaceManager.currentWorkspace.meta.workspaceName })}
            </Typography>
            <br />
            <Typography variant="h6" sx={{
                fontWeight: 200,
            }}>
                {t("patrol:home.quickOptionsTitle")}
            </Typography>
            {/* Quick actions */}
            <List sx={{ width: "100%" }}>
                {/* Add or remove members (owner only) */}
                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <People />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={t("patrol:home.quickActions.addOrRemoveMembers.title")}
                        secondary={<Stack>
                            {t("patrol:home.quickActions.addOrRemoveMembers.description")}
                            <Link>{t("patrol:home.quickActions.addOrRemoveMembers.actionButton")}</Link>
                        </Stack>}
                    />
                </ListItem>
            </List>

            <Typography variant="h6">
                TabID: {this.props.tabID}
            </Typography>
            
            <StateFragment state={testState}>{s =>
                <Button onClick={() => {
                    s.set(s.get() + 1);
                }}>
                    PCount: {s.get()}
                </Button>}
            </StateFragment>

            <Button onClick={() => {
                this.setState({
                    testCount: testRState + 1,
                });
            }}>
                RCount: {testRState}
            </Button>

            <Button onClick={() => {
                TeyoraPatrol.TP.openWikiPage("enwiki", "Vincent van Gogh", true);
            }}>
                Open test page
            </Button>

            {
                // Sound tests
                Object.keys(TeyoraUISoundRecord).map((soundName, i) => {
                    return <Button key={i} onClick={() => {
                        TeyoraUI.playSound(soundName as TeyoraUISounds);
                    }}>
                        Play {soundName}
                    </Button>;
                })
            }


        </Container>;
    }
}

export default withTranslation()(Homepage);