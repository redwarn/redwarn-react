// Wraps MediaWiki parsed HTML in an iframe

import * as React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../../../../App";
import TeyoraUI from "../../../../TeyoraUI";

export default function WikiPageFrame(props: {
    pageHTML: string,
}) {
    const { t } = useTranslation();
    const darkModePref = Teyora.TY.PreferencesManager.getPreference("appearance/useDarkCSSForPages", "respectTheme");

    // Build the page HTML
    const pageHTML = `
           <!DOCTYPE html>
           <html lang="en">
           <head>
               <meta charset="UTF-8">
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
               <base href="${window.location.origin}" /> <!-- This is needed for relative links to work -->
               <title>TY Page Viewer frame</title>
               <!-- Add CSS and JS patches -->
               <script defer type="module" src="/pageviewerframe/script.mjs"></script>
               <link rel="stylesheet" href="/pageviewerframe/style.css" />

               <style>

               ${
    // Dark mode ONLY styling. Respect preferences
    (
        (darkModePref === "respectTheme" && Teyora.TY.state.theme.palette?.mode === "dark") ||
                       darkModePref === "alwaysDark"
    ) ? `
                       @import "https://en.wikipedia.org/w/index.php?title=MediaWiki:Gadget-dark-mode.css&action=raw&ctype=text/css";
                   ` : "" /* If you want lightmode CSS, add it here */}



               /* Add room to breathe (user preference) */
               body {
                   padding: ${Teyora.TY.PreferencesManager.getPreference("appearance/wikiPagePadding", "1rem")}; 
               }
                </style>
           </head>
           <body>
           
               ${props.pageHTML}
           </body>
           </html>
               `;
           
    // Generate a blob URL from the page HTML
    const blobURL = URL.createObjectURL(new Blob([pageHTML], { type: "text/html" }));
           
    // Render the page through an iframe
    return <div className="responive-frame-container" style={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "stretch",
    }}>
        <iframe src={blobURL} style={{
            width: "100%",
            height: "100%",
            border: "none",
            background: "black",
        // eslint-disable-next-line react/no-unknown-property
        }} onLoad={(e) => {
            // Get the URL of the page
            let url = ""; // We set this empty first
            try {
                // We try this as if the fram is inaccesible it will throw an error
                // (probably because the page is blocked by security policies)
                url = (e.target as HTMLIFrameElement).contentWindow.location.href;
            } catch (error) {
                console.warn(error);
            }
            // If the URL is not the same as the page we are loading, return to our blob URL
            if (url !== blobURL) {
                (e.target as HTMLIFrameElement).src = blobURL;
                // Show a toast message
                TeyoraUI.showSnackbar(t("patrol:wikiPage.cantLoadPage"), {
                    variant: "info",
                });
            }
        }}> </iframe>
    </div>;
}