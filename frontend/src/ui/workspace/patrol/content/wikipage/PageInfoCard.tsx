/*
Small card for rendering page info either in the sidebar or page history view

Contains the following info:
Page name and wiki
Latest revision user and timestamp
Page size
Page views

If this is a diff page it'll show that this isn't the latest revision too.
*/

import * as React from "react";
import { Box, Card, Container, Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { AccessTime } from "@mui/icons-material";

export interface PageInfoCardProps {
    isLoading: boolean;
    pageTitle?: string;
    wikiID?: string;
    lastEditorName?: string;
    lastEditTimestamp?: string;
    lastEditORES?: {
        goodfaith: number;
        damaging: number;
    }
    revisionID?: string;
    isDiff?: boolean;
    diffOldRevID?: string;
}

export default function PageInfoCard(props: PageInfoCardProps) {
    // No state here as we are re-rendering the entire card when the page changes
    const { t } = useTranslation();
    const { isLoading, pageTitle, wikiID, lastEditorName, lastEditTimestamp, lastEditORES, revisionID, isDiff, diffOldRevID } = props;


    return <Box>
        <Card>
            <Container sx={{
                paddingTop: "1em",
                paddingBottom: "1em",
            }}>
                <Stack>
                    {/* Todo: Consider if clicking the Wiki name will let you go to different langs of this */}
                    <Typography variant="h5">
                        Page Title
                    </Typography>
                    <Typography variant="body2" sx={{
                        fontVariant: "all-small-caps"
                    }}>
                        @ English Wikipedia
                    </Typography>
                    <Divider sx={{ margin: "1em" }} />
                    {/* List of channels */}
                    <List>
                        <ListItem disablePadding>
                            <ListItemButton>
                                <ListItemIcon>
                                    <AccessTime />
                                </ListItemIcon>
                                <ListItemText primary="Last edited: 24 Oct 2022" />
                            </ListItemButton>
                        </ListItem>
                    </List>
                </Stack>
            </Container>
        </Card>
    </Box>;
}