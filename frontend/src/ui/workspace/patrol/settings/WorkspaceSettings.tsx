// The main WorkspaceSettings view. Each section may be divided off into a separate component.
import * as React from "react";
import { withTranslation } from "react-i18next";
import { Button, Container, Stack, Tab, Tabs, Typography } from "@mui/material";
import { RequiredTabProps, TPTab } from "../baseclasses/Tab";
import TeyoraUI from "../../../TeyoraUI";
import Teyora from "../../../../App";

// Keep a list of our setting groups and their rendering components.
interface SettingsGroup {
    title: string; // i18n key
    render: () => JSX.Element;
}

const settingsGroups: SettingsGroup[] = [
    {
        title: "patrol:settings.general",
        render: () => <Typography>General settings</Typography>,
    }
];

class WorkspaceSettings extends TPTab<unknown, RequiredTabProps, unknown> {
    constructor(props: RequiredTabProps) {
        // Set our base state (nothing for now)
        super(props, {});
    }

    render() {
        const { t } = this.props;

        return <Container sx={{
            paddingTop: "1rem",
        }}>
            <Typography variant="h4">
                {t("patrol:settings.title")}
            </Typography>
            <Typography variant="h6">
                {t("patrol:settings.description")}
            </Typography>

            {/* If user isn't approved */
                Teyora.TY.CurrentUser.isApproved === false ? <Stack>
                    <Typography variant="h6">
                        {t("patrol:settings.notApproved")}
                    </Typography>
                </Stack> : <Stack spacing={2}>
                    <Tabs>
                        <Tab label={t("patrol:settings.workspace")} />
                        <Tab label={t("patrol:settings.user")} />
                        <Tab label={t("patrol:settings.global")} />
                    </Tabs>
                </Stack>}



            <Button onClick={() => TeyoraUI.openPreferences()}>
                Open user prefs
            </Button>



        </Container>;
    }
}

export default withTranslation()(WorkspaceSettings);