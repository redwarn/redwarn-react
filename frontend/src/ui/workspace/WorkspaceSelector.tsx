// This is where the user selects the workspace they'd like to use
import * as React from "react";
import { withTranslation } from "react-i18next";
import { Box, Container, Paper, Typography, Step, StepLabel, StepContent, Grid, Alert, Stack, ButtonBase, Avatar, List, ListItem, ListItemAvatar, ListItemText, ListItemButton, ListItemIcon, IconButton, Button, Tooltip, LinearProgress } from "@mui/material";
import TeyoraUI from "../TeyoraUI";
import { LocalisedButtons } from "../LocalisedButtons";
import Teyora, { ITYWindowProps } from "../../App";
import WorkspaceSelectorAppBar from "./WorkspaceSelectorAppBar";
import "../style/WorkspaceSelector.css";
import { Add, Delete, Edit, ExitToApp, Key, Search, Web, Lock } from "@mui/icons-material";
import WorkspaceCreateDialog from "./WorkspaceCreateDialog";
import { CircularProgress } from "@material-ui/core";
import InputTitleWithHelp from "../components/InputTitleWithHelp";


            

class WorkplaceSelector extends React.Component<ITYWindowProps> {
    state = {
        createWorkspaceDialogOpen: false,
        loadingWorkspaces: true,
        shouldReloadWorkspaces: false,
    };

    render() {
        const { t, i18n } = this.props; // These are filled in by the withTranslation() decorator.

        // Load workspaces asynchronously
        (async () => {
            if (Teyora.TY.WorkspaceManager.hasLoaded && !this.state.shouldReloadWorkspaces) {
                this.setState({ loadingWorkspaces: false });
                return;
            }

            // Load workspaces
            await Teyora.TY.WorkspaceManager.fetchWorkspaceMeta();
            this.setState({ loadingWorkspaces: false, shouldReloadWorkspaces: false });
        })();

        return (
            <Box
                sx={{
                    margin: 0,
                    padding: 0,
                    bgcolor: "background.default",
                    color: "text.primary",
                }}
            >
                <WorkspaceSelectorAppBar />
                
                {/* Main content */}
                <Container maxWidth="lg" sx={{paddingTop: "2vh"}}>
                    <Stack spacing={1}>
                        {/* Top "welcome back" bar */}

                        <Typography variant="h2">
                            {t("ui:workspaceSelector.title", { user: TeyoraUI.getUserRef()})}
                        </Typography>
                        <Typography variant="h5">
                            {t("ui:workspaceSelector.description")}
                        </Typography>

                        {/* Not approved warning */}
                        {!Teyora.TY.CurrentUser.isApproved && (
                            <Alert severity="warning"> 
                                {t("ui:workspaceSelector.notApproved")}
                            </Alert>
                        )}

                        {/* Has the main content with workspaces - todo, on small screens have a list instead */}
                        <Grid spacing={2} container>
                            <Grid item xs={8}>
                                <Paper>
                                    <Box p={2}>
                                        <Typography variant="h6">
                                            {t("ui:workspaceSelector.myWorkspaces")}
                                        </Typography>

                                        {
                                            this.state.loadingWorkspaces ? ( // Loading workspaces
                                                <Box sx={{textAlign: "center"}}>
                                                    <CircularProgress />
                                                </Box>
                                            ) : ( // Workspace List
                                                <List sx={{ width: "100%" }}>
                                                    {Teyora.TY.WorkspaceManager.workspaces.length === 0 ? ( // No workspaces
                                                        <ListItem>
                                                            <ListItemText primary={t("ui:workspaceSelector.noWorkspaces")} />
                                                        </ListItem>
                                                    ) : ( // Workspaces list
                                                        Teyora.TY.WorkspaceManager.workspaces.map((workspace) => ( 
                                                            <ListItem
                                                                key={workspace.teyoraWorkspaceID}
                                                                secondaryAction={
                                                                    <Stack spacing={1} direction="row">
                                                                        {
                                                                            !workspace.workspaceIsLocked ? (
                                                                                <Button variant="outlined" onClick={()=>{
                                                                                    // Open the workspace
                                                                                    Teyora.TY.OpenWorkspace(workspace.teyoraWorkspaceID);
                                                                                }}>
                                                                                    {t("ui:workspaceSelector.openWorkspace")}
                                                                                </Button>
                                                                            ) : (
                                                                                <Tooltip title={t("ui:workspaceSelector.workspaceLocked")}>
                                                                                    <IconButton sx={{
                                                                                        color: "red",
                                                                                        cursor: "not-allowed",
                                                                                    }}>
                                                                                        <Lock />
                                                                                    </IconButton>
                                                                                </Tooltip>
                                                                            )
                                                                        }
                                                                        {/* The leave button also acts as a transfer/delete for the owner of a workspace */}
                                                                        <Tooltip title={
                                                                            workspace.workspaceOwner != Teyora.TY.CurrentUser.userID ?
                                                                                t("ui:workspaceSelector.leaveWorkspace")
                                                                                :
                                                                                t("ui:workspaceSelector.deleteWorkspace")
                                                                        }>
                                                                            <IconButton onClick={
                                                                                () => {
                                                                                    // Leave or delete the workspace
                                                                                    Teyora.TY.WorkspaceManager.leaveOrDeleteWorkspace(workspace.teyoraWorkspaceID);
                                                                                }
                                                                            }>
                                                                                {
                                                                                    workspace.workspaceOwner != Teyora.TY.CurrentUser.userID ?
                                                                                        <ExitToApp />
                                                                                        :   <Delete />
                                                                                }
                                                                            </IconButton>
                                                                        </Tooltip>
                                                                    </Stack>
                                                                }>
                                                                <ListItemAvatar>
                                                                    <Avatar>
                                                                        <Web />
                                                                    </Avatar>
                                                                </ListItemAvatar>
                                                                <ListItemText primary={workspace.workspaceName} secondary={
                                                                    workspace.lastOpenedByUser ?
                                                                        t("ui:workspaceSelector.lastOpenedByYou", { date: workspace.lastOpenedByUser.toLocaleDateString(i18n.language, { weekday: "long", year: "numeric", month: "long", day: "numeric" }) })
                                                                        :
                                                                        t("ui:workspaceSelector.lastOpenedByUnknown", { date: workspace.workspaceLastActive.toLocaleDateString(i18n.language, { weekday: "long", year: "numeric", month: "long", day: "numeric" }) })
                                                                } />
                                                            </ListItem>
                                                        )))}
                                                </List>
                                            )
                                        }
                                    </Box>
                                </Paper>
                            </Grid>

                            {/* Actions: Edit, create, join, etc. */}
                            <Grid item xs={4}>
                                <Stack spacing={2} direction="column">
                                    <Paper>
                                        <Box p={2}>
                                            <Typography variant="h6">
                                                {t("ui:workspaceSelector.getWorkspaces")}
                                            </Typography>
                                            <List>
                                                <ListItem disablePadding>
                                                    <ListItemButton onClick={()=>{
                                                    // Creation is only available to approved users
                                                    // todo: stop creation if more than 100 workspaces joined
                                                        if (Teyora.TY.CurrentUser.isApproved) {
                                                            this.setState({createWorkspaceDialogOpen: true});
                                                        } else {
                                                            TeyoraUI.showAlertBox(t("ui:workspaceSelector.createNotApproved.message"), t("ui:workspaceSelector.createNotApproved.title"));
                                                        }
                                                    }}>
                                                        <ListItemIcon>
                                                            <Add />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.createNewWorkspace")} />
                                                    </ListItemButton>
                                                </ListItem>
                                                <ListItem disablePadding>
                                                    <ListItemButton>
                                                        <ListItemIcon>
                                                            <Search />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.searchStoreWorkspaces")} />
                                                    </ListItemButton>
                                                </ListItem>
                                                <ListItem disablePadding>
                                                    <ListItemButton>
                                                        <ListItemIcon>
                                                            <Key />
                                                        </ListItemIcon>
                                                        <ListItemText primary={t("ui:workspaceSelector.joinPrivate")} />
                                                    </ListItemButton>
                                                </ListItem>
                                            </List>
                                        </Box>
                                    </Paper>
                                    {/* Quota area */}
                                    { (Teyora.TY.WorkspaceManager.hasLoaded) && (
                                        <Paper>
                                            <Box p={2}>
                                                <Stack spacing={2}>
                                                    <InputTitleWithHelp title={t("ui:workspaceSelector.quota")} help={t("ui:workspaceSelector.quotaHelp")} />
                                                    <LinearProgress variant="determinate" value={Teyora.TY.WorkspaceManager.workspaces.length} />
                                                    <Typography variant="body2">
                                                        {Teyora.TY.WorkspaceManager.workspaces.length < 100 ? t("ui:workspaceSelector.quotaInfo", {
                                                            quota: (100 - Teyora.TY.WorkspaceManager.workspaces.length).toString(),
                                                        })
                                                            : t("ui:workspaceSelector.quotaInfoFull")
                                                        }
                                                    </Typography>
                                                </Stack>
                                            </Box>
                                        </Paper>
                                    )}
                                </Stack>
                            </Grid>
                        </Grid>
                    </Stack>
                </Container>

                {/* Dialogs */}
                <WorkspaceCreateDialog open={this.state.createWorkspaceDialogOpen} onClose={()=>{
                    this.setState({createWorkspaceDialogOpen: false, shouldReloadWorkspaces: true, loadingWorkspaces: true});
                }} />
            </Box>
        );
    }
}

export default withTranslation()(WorkplaceSelector);