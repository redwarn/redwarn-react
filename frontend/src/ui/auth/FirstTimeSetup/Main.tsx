import * as React from "react";
import {TYWordmarkPrimaryContrasted} from "../../components/TYLogo";
import "../../style/FirstTimeSetup.css";
import { withTranslation } from "react-i18next";
import { Box, AppBar, Toolbar, Container, Grid, Paper, IconButton, Tooltip } from "@mui/material";
import SelectLangButtonAndDialog from "../../dialogs/SelectLangButtonAndDialog";
import TeyoraUI from "../../TeyoraUI";
import { LocalisedButtons } from "../../LocalisedButtons";
import FTSStepper from "./FTSStepper";
import { Logout } from "@mui/icons-material";


/**
 * The FirstTimeSetup is a Teyora's landing page after a user, featuring a FirstTimeSetup button that leads to a verification page,
 * which is used to request access from a Wikipedia user to use their account for Teyora's tools.
 **/

export default class FirstTimeSetup extends React.Component {

    /**
     * Renders the page.
     **/
    render() : JSX.Element {
        const lB = LocalisedButtons();

        return (
            <Box
                className={"TY-FirstTimeSetup"}
                sx={{
                    margin: 0,
                    padding: 0,
                    bgcolor: "background.default",
                    color: "text.primary",
                }}
            >
                <AppBar position="static" className={"TY-FirstTimeSetup-header"} color="primary" enableColorOnDark={true}>
                    <Toolbar
                        disableGutters={true}
                        className={"TY-FirstTimeSetup-header-toolbar"}
                        color="primary">
                        <div className={"TY-FirstTimeSetup-header-left"}>
                            <TYWordmarkPrimaryContrasted />
                        </div>
                        <div className={"TY-FirstTimeSetup-header-right"}>
                            <SelectLangButtonAndDialog />
                            {/* Logout button */}
                            <Tooltip title={lB.LOGOUT}>
                                <IconButton
                                    onClick={()=>TeyoraUI.confirmLogout()}
                                    color="secondary"
                                >
                                    <Logout />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </Toolbar>
                </AppBar>

                <Container className={"TY-FirstTimeSetup-container"} maxWidth="lg">


                    {/* Main content */}
                    <Grid
                        container
                        spacing={0}
                        direction="column"
                        justifyContent="center"
                        style={{ minHeight:"90vh" }}
                        sx={{
                            bgcolor: "background.default",
                            color: "text.primary",
                        }}
                    >

                        {/* Main content */}
                        <Grid item xs={10}>
                            <Paper elevation={3}>
                                <Box paddingTop={2} paddingBottom={2} sx={{
                                    overflowY: "auto"
                                }}>
                                    <Container maxWidth="lg">
                                        <FTSStepper />
                                    </Container>
                                </Box>
                            </Paper>
                        </Grid>
   
                    </Grid> 
                </Container>
            </Box>
        );
    }
}