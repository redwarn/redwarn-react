/*
This is an important dialog shown once for a first login on a browser
It contains information about the users responsibilities and agreements
*/

import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useTranslation } from "react-i18next";
import { Alert, AlertTitle } from "@mui/material";
import { LocalisedButtons } from "../LocalisedButtons";
import TeyoraUI from "../TeyoraUI";

interface NewUserWarningDialogProps {
    open: boolean;
    onClose: () => void;
    onLoginButtonClick: () => void;
}

export default function NewUserWarningDialog(props: NewUserWarningDialogProps): JSX.Element {
    const { t } = useTranslation();
    const { open, onClose } = props;
    const lB = LocalisedButtons();

    return (
        <Dialog
            open={open}
            onClose={onClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
        >
            <DialogTitle id="scroll-dialog-title">{t("login:newUserWarning.title")}</DialogTitle>
            <DialogContent dividers={true}>
                <DialogContentText
                    tabIndex={-1}
                >
                    {t("login:newUserWarning.text")}
                </DialogContentText>
                <br />
                <Alert
                    severity="info"
                    variant="outlined"
                    action={
                        <Button color="inherit" size="small" onClick={()=>TeyoraUI.showAlertBox("Please tell the devs to add a link to the final TOS here.")}>
                            {lB.OPEN}
                        </Button>
                    }
                >
                    {t("login:newUserWarning.disclaimer")}
                </Alert>
                <br />
                <Alert severity="warning" variant="outlined">
                    <AlertTitle>{t("login:newUserWarning.wmfDisclaimerTitle")}</AlertTitle>
                    {t("login:newUserWarning.wmfDisclaimerContent")}
                </Alert>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>{lB.CANCEL}</Button>
                <Button onClick={props.onLoginButtonClick}>{lB.OK}</Button>
            </DialogActions>
        </Dialog>
    );
}
