import * as React from "react";
import {TeyoraWordmark, TeyoraWordmarkDark, TeyoraWordmarkWhite} from "../../Resources";
import { useTheme } from "@mui/material/styles";

interface LogoProps {
    style?: React.CSSProperties;
    className?: string;
}

/**
 * An image element containing the Teyora wordmark.
 **/
export function TYWordmark(props?: LogoProps) : JSX.Element {
    return <img
        className={`TY-logo TY-logo-increasedSize ${props.className ? props.className : ""}`}
        src={TeyoraWordmark}
        alt={"Teyora logo"}
        style={props.style}
    />;
}

/**
 * An image element containing the Teyora wordmark (for dark backgrounds).
 **/
export function TYWordmarkDark(props?: LogoProps) : JSX.Element {
    return <img className={`TY-logo TY-logo-dark ${props.className ? props.className : ""}`} src={TeyoraWordmarkDark} alt={"Teyora logo"} style={props.style}/>;
}

/**
 * An image element containing the Teyora wordmark (for dark backgrounds).
 **/
export function TYWordmarkWhite(props?: LogoProps) : JSX.Element {
    return <img
        className={`TY-logo TY-logo-increasedSize ${props.className ? props.className : ""}`}
        src={TeyoraWordmarkWhite}
        alt={"Teyora logo"}
        style={props.style}
    />;
}

/**
 * An image element containing the Teyora wordmark (switches depending on theme type).
 **/
export function TYWordmarkAuto(props: LogoProps) : JSX.Element {
    const classes = useTheme();
    if (classes.palette.mode === "dark")
        return TYWordmarkWhite(props); // Don't use the colored logo in UI, it looks terrible
    else
        return TYWordmarkDark(props);
}

/**
 * An image element containing the Teyora wordmark (switches depending on theme type).
 *
 * Haven't actually tested this wordmark yet.
 **/
export function TYWordmarkPrimaryContrasted(props: LogoProps) : JSX.Element {
    const classes = useTheme();

    /* Parse color to RGB */
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    const color = classes.palette.primary.main.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);

    /* Check luminance and return respective logo. */
    if (result) {
        return ((0.2126 * +(parseInt(result[1], 16)))
            + (0.7152 * +(parseInt(result[2], 16)))
            + (0.0722 * +(parseInt(result[3], 16)))) > 128 ?
            TYWordmarkDark(props) : TYWordmarkWhite(props);
    } else
        return TYWordmarkDark(props);
}