// A title that will show above inputs with a help icon that opens an explainatory popover.
import { HelpOutline } from "@mui/icons-material";
import { Stack, Typography, IconButton, Popover } from "@mui/material";
import * as React from "react";

export default function InputTitleWithHelp(props: { title: string, help: string }) {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
    return (
        <Stack direction={"row"}>
            <Typography variant="h6">{props.title}</Typography>
            <IconButton size="small" onClick={e => {
                setAnchorEl(e.currentTarget);
            }}>
                <HelpOutline fontSize="small" />
            </IconButton>
            <Popover
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={() => {setAnchorEl(null);}}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                }}
            >
                <Typography sx={{ p: 2, maxWidth: "20vw" }}>{props.help}</Typography>
            </Popover>
        </Stack>
    );
}