/* eslint-disable no-unused-vars */
// Settings for preferences dialog, these are purely visual
import { HelpOutline } from "@mui/icons-material";
import { FormControl, FormControlLabel, IconButton, InputLabel, MenuItem, Popover, Radio, RadioGroup, Select, SelectChangeEvent, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../../App";
import InputTitleWithHelp from "../../components/InputTitleWithHelp";
import { LocalisedButtons } from "../../LocalisedButtons";
import TeyoraUI from "../../TeyoraUI";

export enum PreferenceType {
    RADIO = "radio",
    CHECKBOX = "checkbox",
    DROPDOWN = "select",
    CUSTOM = "custom",
}

export interface RadioOrCheckboxOption {
    label: string;
    id: string;
    disabled?: boolean;
}

export interface DropdownOption {
    label: string;
    id: string;
}

export interface CustomPreferenceProps {
    // eslint-disable-next-line no-unused-vars
    setValue: (value: any) => void;
    getValue: () => any;
}

export interface Preference {
    // The ID of the preference
    id: string;
    // The name of the preference
    name: string;
    // The description of the preference
    description: string;
    // The type of the preference
    type: PreferenceType;
    // The default value of the preference
    default: any;

    // The options for the preference (if it is a dropdown, radio, or checkbox)
    options?: RadioOrCheckboxOption[] | DropdownOption[];

    // eslint-disable-next-line no-unused-vars
    validate?: (value: any) => string | false; // Return an error message if the value is invalid

    // For custom preferences, this is the component to render
    render?: (props: CustomPreferenceProps) => JSX.Element;

    // Called when the preference is changed
    onSave?: (value: any) => Promise<void>;

}

export interface PreferenceGroup {
    // The ID of the preference group (such as general, appearance, etc)
    groupId: string;

    // The name of the preference group 
    name: string;

    // The description of the preference group
    description: string;

    // The preferences in this group
    preferences: Preference[];
}

export function RenderPreference(props: {pref: Preference, prefGroupId: string}) {
    const key = `${props.prefGroupId}/${props.pref.id}`;
    const [value, setValue] = React.useState(Teyora.TY.PreferencesManager.PreferenceChangesQueue[key] || Teyora.TY.PreferencesManager.getPreference(key, props.pref.default));
    const [error, setError] = React.useState("");

    // Generate a random ID for the preference element
    const id = `pref-${Math.random()}`;

    // Create a render function for each preference type
    if (props.pref.type === PreferenceType.RADIO) {
        props.pref.render = (crProps: CustomPreferenceProps) => {
            return <FormControl>
                <RadioGroup
                    defaultValue={crProps.getValue()}
                    name={id}
                >
                    {(props.pref.options as RadioOrCheckboxOption[])?.map((option) =>{
                        return (
                            <FormControlLabel
                                key={option.id}
                                value={option.id}
                                control={<Radio disabled={option.disabled} onChange={
                                    (e: SelectChangeEvent) => {
                                        crProps.setValue(e.target.value);
                                    }
                                } />}
                                label={option.label}
                                disabled={option.disabled}
                            />
                        );
                    })}
                   
                </RadioGroup>
            </FormControl>;
        };
    } else if (props.pref.type === PreferenceType.CHECKBOX) {
        props.pref.render = (crProps: CustomPreferenceProps) => {
            return <h1>Checkbox option not supported yet</h1>;
        };
    } else if (props.pref.type === PreferenceType.DROPDOWN) {
        props.pref.render = (crProps: CustomPreferenceProps) => {
            return <Box sx={{paddingTop: "1vh", paddingBottom: "1vh"}}>
                <FormControl fullWidth>
                    <InputLabel id={id}>{props.pref.name}</InputLabel>
                    <Select
                        labelId={id}
                        id={`${id}-select`}
                        value={crProps.getValue()}
                        label={props.pref.name}
                        onChange={(event: SelectChangeEvent) => {
                            crProps.setValue(event.target.value);
                        }}
                    >
                        {props.pref.options?.map((option) => {
                            return <MenuItem key={option.id} value={option.id}>{option.label}</MenuItem>;
                        })}
                    </Select>
                </FormControl>
            </Box>;
        };
    }


    return (
        <Stack>
            <InputTitleWithHelp title={props.pref.name} help={props.pref.description} />

            <Typography variant="body2">Debug Value:{value}</Typography>
            

            {/* This is a placeholder */}
            {props.pref.render({
                setValue: (value: any) => {
                    // validate the value
                    if (props.pref.validate) {
                        const error = props.pref.validate(value);
                        if (error) {
                            setError(error);
                            return;
                        }
                    }
                    setError("");
                    setValue(value);
                    Teyora.TY.PreferencesManager.setPreference(`${props.prefGroupId}/${props.pref.id}`, value);
                    
                    
                },
                getValue: () => {
                    return value;
                },
            })}

            <Typography variant="caption" color="error">{error}</Typography>
        </Stack>
    );
}