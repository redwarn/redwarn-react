// These are the default preferences built into Teyora for the preferences dialog.
import { Alert, AlertTitle, Slider, Stack, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { ProfileEntry } from "../../../../../backend/src/db/dbcollections/profile";
import Teyora from "../../../App";
import i18n from "../../../i18n";
import ProfileEditor from "../../components/profile/ProfileEditor";
import TeyoraUI from "../../TeyoraUI";
import { UserTheme } from "../../Themes";
import SelectLangButtonAndDialog from "../SelectLangButtonAndDialog";
import { CustomPreferenceProps, PreferenceGroup, PreferenceType } from "./Preference";

export default class DefaultPreferences {
    // Todo: Add approval group for user to approve their account.


    static generalGroup() : PreferenceGroup {
        const { t } = i18n;
        return {
            groupId: "general",
            name: t("ui:preferences.general.name"),
            description: t("ui:preferences.general.description"),
            preferences: [
                // Language
                {
                    id: "locale",
                    name: t("ui:preferences.general.locale.name"),
                    description: t("ui:preferences.general.locale.description"),
                    type: PreferenceType.CUSTOM,
                    default: i18n.language,
                    render: (crProps: CustomPreferenceProps) => {
                        return <SelectLangButtonAndDialog buttonColor="primary" onChange={async lang=>{
                            crProps.setValue(lang);

                            // TODO: This is bugged due to languages changing
                            await TeyoraUI.showAlertBox(t("ui:preferences.general.locale.alert.message"), t("ui:preferences.general.locale.alert.title"));
                        }} />;
                    }
                },

                // Remind me to take a break every...
                {
                    id: "break_interval",
                    name: t("ui:preferences.general.break_interval.name"),
                    description: t("ui:preferences.general.break_interval.description"),
                    type: PreferenceType.RADIO,
                    default: "30",
                    options: [
                        {
                            id: "never",
                            label: t("ui:preferences.general.break_interval.never"),
                        },
                        {
                            id: "15",
                            label: t("ui:preferences.general.break_interval.options.15"),
                        },
                        {
                            id: "30",
                            label: t("ui:preferences.general.break_interval.options.30"),
                        },
                        {
                            id: "45",
                            label: t("ui:preferences.general.break_interval.options.45"),
                        },
                        {
                            id: "60",
                            label: t("ui:preferences.general.break_interval.options.60"),
                        },
                        {
                            id: "90",
                            label: t("ui:preferences.general.break_interval.options.90"),
                        }
                    ],

                    validate: (value: any) => {
                        return !Teyora.TY.CurrentUser.isApproved && value !== "30" && value !== "15"
                            ? t("ui:preferences.approvalNeededForPref") as string
                            : false;
                    }
                }
            ]
        };
    }

    static appearanceGroup() : PreferenceGroup {
        const { t } = i18n;
        return {
            groupId: "appearance",
            name: t("ui:preferences.appearance.name"),
            description: t("ui:preferences.appearance.description"),
            preferences: [
                {
                    id: "theme",
                    name: t("ui:preferences.appearance.theme.name"),
                    description: t("ui:preferences.appearance.theme.description"),
                    type: PreferenceType.DROPDOWN,
                    default: "teyora", // Todo
                    options: [
                        {
                            id: "teyora",
                            label: t("ui:preferences.appearance.theme.teyora")
                        },
                        {
                            id: "monotone",
                            label: t("ui:preferences.appearance.theme.monotone")
                        },
                        {
                            id: "redwarning",
                            label: t("ui:preferences.appearance.theme.redwarning")
                        }
                    ],

                    // Validate is called on every change, so we set the theme here.
                    validate: (value: string) => {
                        Teyora.TY.setState({ theme: UserTheme({
                            userPreviewThemePrefKey: value,
                            userPreviewDarkModePref: Teyora.TY.PreferencesManager.PreferenceChangesQueue["appearance/themeVariant"] || null,
                        }) });
                        return false;
                    }
                    
                },

                {
                    id: "themeVariant",
                    name: t("ui:preferences.appearance.themeVariant.name"),
                    description: t("ui:preferences.appearance.themeVariant.description"),
                    type: PreferenceType.RADIO, 
                    default: "system", // Todo
                    options: [
                        {
                            id: "system",
                            label: t("ui:preferences.appearance.themeVariant.system")
                        },
                        {
                            id: "light",
                            label: t("ui:preferences.appearance.themeVariant.light")
                        },
                        {
                            id: "dark",
                            label: t("ui:preferences.appearance.themeVariant.dark")
                        }
                    ],

                    // Validate is called on every change, so we set the theme here.
                    validate: (value: string) => {
                        Teyora.TY.setState({ theme: UserTheme({
                            userPreviewThemePrefKey: Teyora.TY.PreferencesManager.PreferenceChangesQueue["appearance/theme"] || null,
                            userPreviewDarkModePref: value, 
                        }) });
                        return false;
                    }
                },

                {
                    id: "useDarkCSSForPages",
                    name: t("ui:preferences.appearance.useDarkCSSForPages.name"),
                    description: t("ui:preferences.appearance.useDarkCSSForPages.description"),
                    type: PreferenceType.RADIO,
                    default: "respectTheme", 
                    options: [
                        {
                            id: "respectTheme",
                            label: t("ui:preferences.appearance.useDarkCSSForPages.respectTheme")
                        },
                        {
                            id: "alwaysLight",
                            label: t("ui:preferences.appearance.useDarkCSSForPages.alwaysLight")
                        },
                        {
                            id: "alwaysDark",
                            label: t("ui:preferences.appearance.useDarkCSSForPages.alwaysDark")
                        }
                    ]
                },

                {
                    id: "wikiPagePadding",
                    name: t("ui:preferences.appearance.wikiPagePadding.name"),
                    description: t("ui:preferences.appearance.wikiPagePadding.description"),
                    type: PreferenceType.RADIO,
                    default: "1rem",
                    options: [
                        {
                            id: "0",
                            label: t("ui:preferences.appearance.wikiPagePadding.options.0")
                        },
                        {
                            id: "0.5rem",
                            label: t("ui:preferences.appearance.wikiPagePadding.options.05rem")
                        },
                        {
                            id: "1rem",
                            label: t("ui:preferences.appearance.wikiPagePadding.options.1rem")
                        },
                        {
                            id: "2rem",
                            label: t("ui:preferences.appearance.wikiPagePadding.options.2rem")
                        },
                        {
                            id: "3rem",
                            label: t("ui:preferences.appearance.wikiPagePadding.options.3rem")
                        }
                    ]
                }
            ]
        };
    }

    static profileGroup() : PreferenceGroup {
        const { t } = i18n;
        return {
            groupId: "profile",
            name: t("ui:preferences.profile.name"),
            description: t("ui:preferences.profile.description"),
            preferences: [
                {
                    id: "profile",
                    name: t("ui:preferences.profile.profile.name"),
                    description: t("ui:preferences.profile.profile.description"),
                    type: PreferenceType.CUSTOM,
                    default: "", // Todo
                    render: () => {
                        return <div>
                            <Alert severity="info" variant="outlined">
                                {t("ui:preferences.profile.profile.updateInfo")}
                            </Alert>
                            <ProfileEditor
                                user={Teyora.TY.CurrentUser}
                                profile={Teyora.TY.CurrentProfile}
                                onSave={async (profile: ProfileEntry) => {
                                    Teyora.TY.SetFullPageLoaderOpen(true);
                                    try {
                                        await Teyora.TY.authManager.updateProfile(profile.profile);
                                        Teyora.TY.SetFullPageLoaderOpen(false);
                                        await TeyoraUI.showAlertBox(t("ui:preferences.profile.profile.updateSuccess"), t("ui:preferences.profile.profile.updateSuccessTitle"));
                                    } catch (error) {
                                        Teyora.TY.SetFullPageLoaderOpen(false);
                                        TeyoraUI.showAlertBox(
                                            t("login:firstTimeSetup.profile.error.message"),
                                            t("login:firstTimeSetup.profile.error.title")
                                        );
                                        return;
                                    }
                                }}
                            />
                        </div>;
                    }
                }
            ]
        };
    }

    static performanceGroup() : PreferenceGroup {
        const { t } = i18n;
        return {
            groupId: "performance",
            name: t("ui:preferences.performance.name"),
            description: t("ui:preferences.performance.description"),
            preferences: [
                {
                    id: "feedThrottling",
                    name: t("ui:preferences.performance.feedThrottling.name"),
                    description: t("ui:preferences.performance.feedThrottling.description"),
                    type: PreferenceType.RADIO,
                    default: "enabled", // Todo
                    validate: () => {
                        // Restrict this to approved users only
                        if (!Teyora.TY.CurrentUser.isApproved) {
                            return t("ui:preferences.approvalNeededForPref") as string;
                        }
                    },

                    options: [
                        {
                            id: "enabled",
                            label: t("ui:preferences.performance.feedThrottling.enabled")
                        },
                        {
                            id: "disabled",
                            label: t("ui:preferences.performance.feedThrottling.disabled")
                        }
                    ]
                },

                {
                    id: "feedThrottlingDelayMin",
                    name: t("ui:preferences.performance.feedThrottlingDelayMin.name"),
                    description: t("ui:preferences.performance.feedThrottlingDelayMin.description"),
                    type: PreferenceType.CUSTOM,
                    default: 300,
                    validate: (value: number) => {
                        // The minimum value should not be changed by non-approved users
                        if (!Teyora.TY.CurrentUser.isApproved) {
                            return t("ui:preferences.approvalNeededForPref") as string;
                        }

                        // Get the max value (either the one that is being set or from prefs)
                        const max = Teyora.TY.PreferencesManager.PreferenceChangesQueue["performance/feedThrottlingDelayMax"] ||
                            Teyora.TY.PreferencesManager.getPreference("performance/feedThrottlingDelayMax", 1000);
                        return value > max ? t("ui:preferences.performance.feedThrottlingDelayMin.overmaxError") as string : false;
                    },

                    render: (crProps: CustomPreferenceProps) => {
                        // A slider from 100ms to 5s
                        return <Stack>
                            <Slider
                                value={crProps.getValue()}
                                onChange={(event, value) => {
                                    crProps.setValue(value);
                                }}
                                step={100}
                                marks
                                min={100}
                                max={5000}
                            />
                            <Typography variant="caption">
                                {crProps.getValue()} ms
                            </Typography>
                        </Stack>;
                    }
                },

                {
                    id: "feedThrottlingDelayMax",
                    name: t("ui:preferences.performance.feedThrottlingDelayMax.name"),
                    description: t("ui:preferences.performance.feedThrottlingDelayMax.description"),
                    type: PreferenceType.CUSTOM,
                    default: 1000,

                    validate: (value: number) => {
                        // Any user can change the max
                        // Get the min value (either the one that is being set or from prefs)
                        const min = Teyora.TY.PreferencesManager.PreferenceChangesQueue["performance/feedThrottlingDelayMin"] ||
                            Teyora.TY.PreferencesManager.getPreference("performance/feedThrottlingDelayMin", 300);
                        return value < min ? t("ui:preferences.performance.feedThrottlingDelayMax.underminError") as string : false;
                    },

                    render: (crProps: CustomPreferenceProps) => {
                        // A slider from 100ms to 5s
                        return <Stack>
                            <Slider
                                value={crProps.getValue()}
                                onChange={(event, value) => {
                                    crProps.setValue(value);
                                }}
                                aria-labelledby="discrete-slider-always"
                                step={100}
                                marks
                                min={100}
                                max={5000}
                            />
                            <Typography variant="caption">
                                {crProps.getValue()} ms
                            </Typography>
                        </Stack>;
                    }
                },

            ]
        };
    }

    // Debug group (ALWAYS LAST)
    // For debuggers only - enable through #debugprefs in the URL or enabling it in prefs
    static debugGroup() : PreferenceGroup {
        const { t } = i18n;
        return {
            groupId: "debug",
            name: t("ui:preferences.debug.name"),
            description: t("ui:preferences.debug.description"),
            preferences: [
                {
                    id: "debugModeEnabled",
                    name: t("ui:preferences.debug.debugModeEnabled.name"),
                    description: t("ui:preferences.debug.debugModeEnabled.description"),
                    type: PreferenceType.RADIO,
                    default: "disabled",
                    validate: () => {
                        // Show warning dialog
                        TeyoraUI.showAlertBox(
                            t("ui:preferences.debug.debugModeEnabled.warning"),
                            t("ui:preferences.debug.debugModeEnabled.warningTitle")
                        );

                        return false;
                    },

                    options: [
                        {
                            id: "enabled",
                            label: t("ui:preferences.debug.debugModeEnabled.enabled")
                        },
                        {
                            id: "disabled",
                            label: t("ui:preferences.debug.debugModeEnabled.disabled")
                        }
                    ]
                },

                // Custom preference that just shows text
                // {
                //     id: "debugModeText",
                //     name: t("ui:preferences.debug.debugModeText.name"),
                //     description: t("ui:preferences.debug.debugModeText.description"),
                //     type: PreferenceType.CUSTOM,
                //     default: "",
                //     render: (crProps: CustomPreferenceProps) => {
                //         return <Typography variant="body1">
                //             I think... <br/>
                //             User information is as follows: <br/>
                //             <ul>
                //                 {Object.keys(Teyora.TY.CurrentUser).map(key => {
                //                     return <li key={key}>{key}: {
                //                         Teyora.TY.CurrentUser[key] && "☑️"
                //                     }</li>;
                //                 }).reduce((prev, curr) => [prev, " ", curr])}
                //             </ul>

                //         </Typography>;
                //     }
                // }
            ]
        };
    }



    // Gets the preferences the user can see with their permissions
    static getElegiblePreferences() : PreferenceGroup[] {
        return [
            this.generalGroup(),
            this.appearanceGroup(),
            this.profileGroup(),
            this.performanceGroup(),

            // Debug group
            window.location.hash == "#debugprefs" && this.debugGroup(),
        ];
    }
}