import { MessageBoxProps } from "./MessageBox";

export default class MessageBoxQueue {
    public queue: MessageBoxProps[] = [];

    // Gets the next message box in the queue
    public getNextMessageBox(): MessageBoxProps | undefined {
        return this.queue.shift();
    }

    public enqueueMessageBox(props: MessageBoxProps): void {
        this.queue.push(props);
        this.queueUpdate();
    }

    // This is set by the MessageBox component
    // eslint-disable-next-line no-unused-vars
    public queueUpdate: () => void;
}