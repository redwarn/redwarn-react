/*
Teyora Default Feed Extension
(c) 2022 Ed6767 and contributors
*/

// Add Teyora extension framework
import FeedExtension from './tef/FeedExtension.mjs';
export default class TeyoraFeedExtension extends FeedExtension {
    stream = null;
    rcStream = null; // Where we get the diff size from
    oresStream = null;

    /**
     * We have three queues here:
     * 0. General waiting
     *    Contains the feed objects to be modified by the other queues
     * 
     * 1. Awaiting diff information
     *    Contains diff sizes
     *    
     * 2. Awaiting revision score information
     *    This is only used if ORES is supported and is based on the scores being reported on the
     *    event feed
     */

    // General waiting - key = edit ID, value = feed object
    generalWaiting = {};

    // Key = edit ID, value = size
    editsAwaitingDiffInfo = {};

    // Key = edit ID, value = time added to queue
    editsAwaitingORES = {};

    async init() {
        const { Teyora, TeyoraUI, ActiveWikis } = this;
        
        // Connect to stream
        this.stream = new EventSource("https://stream.wikimedia.org/v2/stream/mediawiki.revision-create");
        this.stream.onmessage = event=>this.onmessage(event); // pass through to preserve this
        this.stream.onerror = ()=>this.ExtensionFail("Error connecting to stream");
        this.stream.onopen = ()=>{
            this.TeyoraUI.showSnackbar("Connected to stream", {
                variant: "success"
            });
        };  

        // Connect to RC stream
        this.rcStream = new EventSource("https://stream.wikimedia.org/v2/stream/recentchange");
        this.rcStream.onmessage = event=>this.onRCmessage(event); // pass through to preserve this
        this.rcStream.onerror = ()=>this.ExtensionFail("Error connecting to RC stream");
        this.rcStream.onopen = ()=>{
            this.TeyoraUI.showSnackbar("Connected to RC stream", {
                variant: "success"
            });
        };

        
        this.oresStream = new EventSource("https://stream.wikimedia.org/v2/stream/mediawiki.revision-score");
        this.stream.onerror = ()=>this.ExtensionFail("ORES scores not available");
        this.stream.onopen = ()=>{
            this.TeyoraUI.showSnackbar("Connected! ORES scores available.", {
                variant: "success"
            });
        };  
        this.oresStream.onmessage = event=>this.onORESmessage(event); // pass through to preserve this
        

        // If we hare not connected over HTTP/2, show a warning
        if (performance.getEntries()[0].nextHopProtocol != "h2") {
            this.TeyoraUI.showSnackbar("Warning: Not connected over HTTP/2 - limit of two tabs.", {
                variant: "warning"
            });
        }


        return; // All done
    }

    async onmessage(event) {
        const { Teyora, TeyoraUI, ActiveWikis } = this;

        const data = JSON.parse(event.data);

        // Don't run if not needed
        if (!ActiveWikis.includes(data.database)) return;

        // Let's convert this into a feed edit
        const feedEdit = {
            id: data.rev_id,
            wikiId: data.database,
            pageTitle: data.page_title,
            user: data.performer.user_text,
            userEditCount: data.performer.user_edit_count,
            summary: data.comment,
            minor: data.rev_minor_edit,
            bot: data.performer.user_groups.includes("bot"),
            timestamp: new Date(data.rev_timestamp),
            diff: 0, // Set later on
        };

        // Know how to handle this event - if ORES is supported we need to cache it until we
        // recieve the revision score from ORES
        const supportsORES = (
            Teyora.WikiSupport[data.database].supportMap &&
            Teyora.WikiSupport[data.database].supportMap.supportsORES == 4
        ) || false;

        // Add to general waiting
        this.generalWaiting[data.database + data.rev_id] = feedEdit;

        // Add to ORES queue if needed
        if (supportsORES) {
            this.editsAwaitingORES[data.database + data.rev_id] = new Date();
        } else {
            // Try to dispatch if RC has beaten us
            this.dispatchEdit(data.database + data.rev_id);
        }
    }

    async onRCmessage(event) {
        const { ActiveWikis } = this;
        const data = JSON.parse(event.data);
        ////console.log(data);
        
        // If not an active wiki or data isn't valid, don't run
        if (!ActiveWikis.includes(data.wiki) || !data.length) return;

        let sizeDiff = data.length.new - data.length.old;
        if (isNaN(sizeDiff)) {
            sizeDiff = data.length.new;
            this.generalWaiting[data.wiki + data.id].isNewPage = true;
        }

        // Add to diff queue
        this.editsAwaitingDiffInfo[data.wiki + data.revision.new] = sizeDiff;

        // Attempt to dispatch
        this.dispatchEdit(data.wiki + data.revision.new);
    }

    async onORESmessage(event) {
        const { ActiveWikis } = this;
        const data = JSON.parse(event.data);
        // Don't run if not needed
        if (!ActiveWikis.includes(data.database)) return;

        if (!this.generalWaiting[data.database + data.rev_id]) return;

        // Update the edit with the ORES score
        if (data.scores) {
            this.generalWaiting[data.database + data.rev_id].oresDamaging = data.scores.damaging.probability.true;
            this.generalWaiting[data.database + data.rev_id].oresGoodFaith = data.scores.goodfaith.probability.true;
        }

        // Remove from the ORES queue
        delete this.editsAwaitingORES[data.database + data.rev_id];

        // Dispatch the edit
        this.dispatchEdit(data.database + data.rev_id);
    }

    dispatchEdit(editID) {
        //console.log(`${Object.keys(this.editsAwaitingDiffInfo).length} edits awaiting diff info, ${Object.keys(this.editsAwaitingORES).length} awaiting ORES, ${Object.keys(this.generalWaiting).length} awaiting general`);

        // If it is in either the ORES or diff queues, don't do anything
        if (
            this.editsAwaitingDiffInfo[editID] == null ||
            this.generalWaiting[editID] == null ||
            this.editsAwaitingORES[editID] != null
        ) return;

        //console.log("passed dispatch");

        // Set the diff info
        this.generalWaiting[editID].diff = this.editsAwaitingDiffInfo[editID];
        
        //console.log(`dispaching - latency: ${new Date().getTime() - this.generalWaiting[editID].timestamp.getTime()}`);
        // Send the edit off and remove it from the general queue
        this.NewFeedEdit(this.generalWaiting[editID]);
        delete this.generalWaiting[editID];
    }
}