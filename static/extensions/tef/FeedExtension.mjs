/**
 * Teyora Extension Framework
 * (c) 2020-2022 Teyora, Ed6767, RedWarn Team
 * 
 * Defines the framework of a Feed Extension
 * This should be extended by another class
 */

export default class FeedExtension {
    Teyora = null;
    TeyoraUI = null;
    TeyoraPatrol = null;
    MUI = null;
    ExtensionFail = null;
    NewFeedEdit = null;
    ActiveWikis = null;

    constructor(imports) {
        this.Teyora = imports.Teyora.TY;
        this.TeyoraUI = imports.TeyoraUI;
        this.TeyoraPatrol = imports.TeyoraPatrol.TP;
        this.MUI = imports.MUI;
        this.ActiveWikis = imports.ActiveWikis;
    }

}