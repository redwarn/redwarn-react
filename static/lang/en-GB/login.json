{
  "title": "Welcome to $t(ui:teyora)",
  "welcome": "$t(ui:teyora) is the web based patrol tool you've always wanted",
  "welcome2": "Get started today by logging in with your Wikimedia account. No downloads or special permissions required.",
  "disclaimer": "You are responsible for all edits you make with $t(ui:teyora). By logging in, you agree to our Terms of Service and Privacy Policy. A restricted mode is available for any editor with auto-confirmed status on any Wikimedia project. Full access can be obtained by request, or by having rollback or administrator rights on any Wikimedia project.",
  "serviceProviders": {
    "wikimedia": "Login with Wikimedia"
  },
  "siteNotices": {
    "newVisitor": "$t(ui:teyora) is a tool for patrolling Wikimedia projects, such as Wikipedia. Want to get started with content moderation? Click here.",
    "loginError": "Login failed",
    "suspendedAccount": "Your account is suspended from Teyora. Check your talk page for more information on how to appeal.",
    "renamedAccountIssue": "You can't log in with this account as your account has been renamed or usurped in a way that conflicts with an existing user. For security reasons, you cannot log in until the conflicting $t(ui:teyora) account has been deleted or renamed. You should contact a $t(ui:teyora) administrator regarding this issue so that they can delete or rename the original account so you may log in.",
    "prereleaseSoftwareWarning": "This is a pre-release service that is available by invite only. You will encounter bugs and other issues - we highly recommend you turn error reporting on when prompted and report any bugs you find."
  },
  "newUserWarning": {
    "title": "First time here?",
    "text": "We noticed this is the first time you've used $t(ui:teyora) on this browser. Please take a moment to read the following information before you start using $t(ui:teyora):",
    "disclaimer": "You are responsible for all edits you make with $t(ui:teyora). By logging in, you agree to our Terms of Service and Privacy Policy.",
    "wmfDisclaimerTitle": "Disclaimer regarding your use of Wikimedia Cloud Services",
    "wmfDisclaimerContent": "Do not use the Cloud Services Project (this site) if you do not agree to the following: information shared with the Cloud Services Project will be made available to volunteer administrators and may not be treated confidentially. Volunteers may have full access to the systems hosting the projects, allowing them access to any data or other information you submit. By creating an account in this project and/or using other Wikimedia Cloud Services Services, you agree that the volunteer administrators of this project will have access to any data you submit. Since access to this information by volunteers is fundamental to the operation of Cloud Services, these terms regarding use of your data expressly override the Wikimedia Foundation's Privacy Policy as it relates to the use and access of your personal information."
  },
  "firstTimeSetup": {
    "stepSummaries": "Welcome/Verify your eligibility/Create your profile/Get Started",
    "doneButton": "Let's go!",
    "welcome": "Welcome to $t(ui:teyora), {{user}}!",
    "welcomeDesc": "You're nearly ready to go. The next few steps will:",
    "SuperOverlordWarning": {
      "title": "Important Security Warning: You are the SuperOverlord",
      "text": "As you are the first to log into this $t(ui:teyora) instance, your account has been assigned the user ID \"SuperOverlord\". As it is assumed you are the instance owner and have direct database access irregardless, the SuperOverlord is provided with additional powers and tools to assign users with privileges and access otherwise hidden information.\n\nIf you did not intend to use this account as the SuperOverlord, log out and remove the corresponding user document from the database using Fauxton."
    },
    "eligibilityDescription": "Verify your eligibility to use $t(ui:teyora) and approve access to advanced tools",
    "profileDescription": "Set up your $t(ui:teyora) profile with a nickname, tags and profile picture to identify yourself with other $t(ui:teyora) editors",
    "finishDescription": "Complete optional steps like securing access to $t(ui:teyora) with two-factor authentication, opening a sandbox workspace to learn $t(ui:teyora)'s user interface and more.",
    "eligibility": {
      "title": "Verify your eligibility",
      "desc": "To use $t(ui:teyora) you need to be at least auto-confirmed on any Wikimedia Wiki. Please select the Wiki where you have the most user rights to use for verification. For example, if you are an administrator or have rollback on a Wiki, select that Wiki.",
      "wikiNotSupported": {
        "message": "Sorry, but you can't use this Wiki for auto-approval or to approve your eligibility. Try another Wiki, or if you're not eligible yet, keep editing and come back soon.",
        "title": "This wiki can't be used"
      },
      "error": {
        "title": "Verification failed",
        "message": "An error occurred whilst trying to verify your account using this wiki. The most likely cause is that you are not registered or have made any edits. Please try again, and if the error persists you may need to log out of $t(ui:teyora) and log in again."
      },
      "notEligible": {
        "message": "Thank you for your interest, however, your eligibility for $t(ui:teyora) could not be verified with this Wiki. Please select another one, or if you are not yet eligible, get more experience with editing and come back later. More information regarding eligibility is available on our Meta Wiki page.",
        "title": "Not Eligible"
      },
      "alreadyUnlocked": "Your account is already eligible for $t(ui:teyora)! However, you haven't been approved for full access. If you have rollback on a Wiki, you can obtain full access by selecting it below. If not, you can request approval after you've used $t(ui:teyora) for a while.",
      "alreadyApproved": "Your account has full access to $t(ui:teyora) - you don't need to do anything."
    },
    "profile": {
      "bypassAttempted": "You cannot bypass the eligibility step. Eligibility is required by the server. If you need help with eligibility, please ask on the talk page instead of attempting to bypass protections.",
      "title": "Set up your profile",
      "desc": "You're nearly done! Click each element to set up your profile with a picture, nickname and short bio so that other $t(ui:teyora) users know who you are and what you're up to.",
      "AccountApproved": "Congratulations! You are eligible for Teyora and have been automatically approved to access the full feature set.",
      "AccountNotApproved": "Your account is eligible for $t(ui:teyora)! However, you haven't been approved for full access. If you have rollback on a Wiki, you can obtain full access automatically by going back to the previous step and selecting a different Wiki. If not, you can request approval after you've used Teyora for a while.",
      "error": {
        "message": "An error occurred whilst attempting to save your profile. Please make sure all information is valid, then try again.",
        "title": "Error saving profile"
      }
    },
    "complete": "Let's go!",
    "finish": {
      "title": "{{user}}, welcome to $t(ui:teyora)!",
      "desc": "You've completed the setup process and you're ready to start editing! Why not check out these additional recommended steps and tutorials?",
      "customise": {
        "title": "Customise",
        "desc": "Make $t(ui:teyora) your own with custom colour schemes and hotkeys"
      },
      "learnHowButton": "Learn How",
      "learn": {
        "title": "Learn",
        "desc": "Learn the $t(ui:teyora) interface without making edits with a sandbox workspace"
      },
      "migrate": {
        "title": "Coming from Twinkle, Huggle, RedWarn or Ultraviolet?",
        "desc": "Learn what makes $t(ui:teyora) different and how to optimise your workflow with $t(ui:teyora)"
      }
    }
  },
  "confirmLogout": {
    "title": "Log out of {{user}}?",
    "text": "Are you sure you want to log out of $t(ui:teyora)?"
  },
  "loginRequiredForLink": "The link you have just followed requires you to be logged in to $t(ui:teyora). Please log in with an elegible Wikimedia account to view this content."
}
